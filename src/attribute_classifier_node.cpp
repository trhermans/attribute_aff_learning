/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2010, Georgia Institute of Technology
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Georgia Institute of Technology nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

// ROS
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/CvBridge.h>
#include <ros/package.h>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// Visual features
#include <cpl_visual_features/sliding_window.h>
#include <cpl_visual_features/features/attribute_learning_base_feature.h>

// Services
#include <attribute_aff_learning/SaveImage.h>
#include <attribute_aff_learning/SetupExampleFiles.h>
#include <attribute_aff_learning/ExtractFeatures.h>

// STL
#include <vector>
#include <string>
#include <sstream>
#include <fstream>

// Boost
#include <boost/shared_ptr.hpp>

using cv::Mat;
using std::string;
using boost::shared_ptr;
using cpl_visual_features::SlidingWindowDetector;
using cpl_visual_features::AttributeLearningBaseFeature;
using namespace attribute_aff_learning;

class AttributeClassifierNode
{
 public:
  // Constructors and Destructors
  AttributeClassifierNode(ros::NodeHandle &n) :
      n_(n), it_(n), wide_save_count_(0), zoom_save_count_(0)
  {
    ros::NodeHandle n_private("~");
    const string default_wide_path = "/tmp/data/robot_images/wide/";
    const string default_zoom_path = "/tmp/data/robot_images/zoom/";
    std::string package_path = ros::package::getPath( "cpl_visual_features");
    std::stringstream texton_path;
    std::stringstream sift_path;
    texton_path << package_path << "/cfg/textons-500.txt";
    sift_path << package_path << "/cfg/sift-center-512.txt";
    const string default_texton_file = texton_path.str();
    const string default_sift_file = sift_path.str();
    n_private.param("wide_save_path", wide_save_path_, default_wide_path);
    n_private.param("zoom_save_path", zoom_save_path_, default_zoom_path);
    n_private.param("img_quality", img_quality_, 0);
    n_private.param("texton_file", texton_file_, default_texton_file);
    n_private.param("sift_file", sift_file_, default_sift_file);


    // Subscribers
    image_sub_ = it_.subscribe("image_topic", 1,
                               &AttributeClassifierNode::imageCallback, this);
    // Publishers

    // Service servers
    save_wide_ = n.advertiseService("save_wide",
                                    &AttributeClassifierNode::saveWideImage,
                                    this);
    save_zoom_ = n.advertiseService("save_zoom",
                                    &AttributeClassifierNode::saveZoomImage,
                                    this);
    dp_feature_extractor_ = n.advertiseService("dp_feature_extractor",
                                               &AttributeClassifierNode::
                                               extractDPFeatures, this);
    att_feature_extractor_ = n.advertiseService("attribute_feature_extractor",
                                                &AttributeClassifierNode::
                                                extractAttFeatures, this);
    ca_feature_extractor_ = n.advertiseService("ca_feature_extractor",
                                               &AttributeClassifierNode::
                                               extractCAFeatures, this);
    write_dp_example_ = n.advertiseService("dp_example_writer",
                                           &AttributeClassifierNode::
                                           setupDPExample, this);
    write_att_example_ = n.advertiseService("attribute_example_writer",
                                            &AttributeClassifierNode::
                                            setupAttExample, this);
    write_ca_example_ = n.advertiseService("ca_example_writer",
                                           &AttributeClassifierNode::
                                           setupCAExample, this);
    // Load texton file
    attribute_wd_.feature_.setTextonFile(texton_file_);
    attribute_wd_.feature_.setSIFTFile(sift_file_);
  }

  /**
   * Callback method to the image transport subscriber. Saves the image to the
   * disk using the base_path described in the parameter server.
   *
   * @param msg_ptr Most recent image off of the topic
   */
  void imageCallback(const sensor_msgs::ImageConstPtr& msg_ptr)
  {
    Mat tmp_frame;
    tmp_frame = bridge_.imgMsgToCv(msg_ptr);
    tmp_frame.copyTo(frame_);
  }

  /**
   * Service server function for saving a wide view image.
   *
   * @param req The service request parameter
   * @param res The service response parameter
   *
   * @return true if the image is succesfully saved, false otherwise.
   */
  bool saveWideImage(SaveImage::Request &req, SaveImage::Response &res)
  {
    std::stringstream file_name;
    file_name << wide_save_path_ << wide_save_count_ << ".png";

    if( saveImage(file_name.str(), req.flip_rgb) )
    {
      res.save_count = wide_save_count_;
      wide_save_count_++;
      return true;
    }
    return false;
  }

  /**
   * Service server function for saving a zoom view image.
   *
   * @param req The service request parameter
   * @param res The service response parameter
   *
   * @return true if the image is succesfully saved, false otherwise.
   */
  bool saveZoomImage(SaveImage::Request &req, SaveImage::Response &res)
  {
    std::stringstream file_name;
    file_name << zoom_save_path_ << zoom_save_count_ << ".png";

    if( saveImage(file_name.str(), req.flip_rgb) )
    {
      res.save_count = zoom_save_count_;
      zoom_save_count_++;
      return true;
    }
    return false;
  }

  /**
   * Take the latest image off of the image topic and write its features to an
   * example file for use with svm_light for direct perception affordance
   * learning.
   *
   * @param req The service request
   * @param res The service response
   *
   * @return true if succesfully extract, false otherwise
   */
  bool extractDPFeatures(ExtractFeatures::Request &req,
                         ExtractFeatures::Response &res)
  {
    // Extract features
    SlidingWindowDetector<AttributeLearningBaseFeature,
        AttributeLearningBaseFeature::Descriptor> attribute_wd;
    attribute_wd.feature_.setTextonFile(texton_file_);
    attribute_wd.feature_.setSIFTFile(sift_file_);

    attribute_wd.scanImage(frame_, std::pair<int,int>(frame_.cols - 1,
                                                      frame_.rows - 1), true);
    std::vector<float> ab_desc = attribute_wd.descriptors_[0];

    // Convert to output format
    std::stringstream feature_stream;
    for (unsigned int j = 0; j < ab_desc.size(); ++j)
    {
      if (ab_desc[j] != 0.0f)
      {
        // SVM Light requires 1 based feature indexing
        feature_stream << " " << (j+1) << ":" << ab_desc[j];
      }
    }
    std::string feature_str = feature_stream.str();

    // Write files
    for (unsigned int a = 0; a < req.affordance_labels.size(); ++a)
    {
      std::stringstream out_fname;
      out_fname << req.example_path << req.affordance_labels[a] << "-test.txt";
      res.example_files.push_back(out_fname.str());
      std::ofstream out_file(out_fname.str().c_str());
      out_file << "+1" << feature_stream.str() << std::endl;
      out_file.close();
    }

    return true;
  }

  /**
   * Take the latest image off of the image topic and write its features to an
   * example file for use with svm_light for attribute based affordance learning.
   *
   * @param req The service request
   * @param res The service response
   *
   * @return true if succesfully extract, false otherwise
   */
  bool extractAttFeatures(ExtractFeatures::Request &req,
                          ExtractFeatures::Response &res)
  {
    // Extract features
    SlidingWindowDetector<AttributeLearningBaseFeature,
        AttributeLearningBaseFeature::Descriptor> attribute_wd;
    attribute_wd.feature_.setTextonFile(texton_file_);
    attribute_wd.feature_.setSIFTFile(sift_file_);

    attribute_wd.scanImage(frame_, std::pair<int,int>(frame_.cols - 1,
                                                      frame_.rows - 1), true);
    std::vector<float> ab_desc = attribute_wd.descriptors_[0];

    // Convert to output format
    std::stringstream feature_stream;
    for (unsigned int j = 0; j < ab_desc.size(); ++j)
    {
      if (ab_desc[j] != 0.0f)
      {
        // SVM Light requires 1 based feature indexing
        feature_stream << " " << (j+1) << ":" << ab_desc[j];
      }
    }
    std::string feature_str = feature_stream.str();

    // Write files
    for (unsigned int a = 0; a < req.attribute_labels.size(); ++a)
    {
      std::stringstream out_fname;
      out_fname << req.example_path << req.attribute_labels[a] << "-test.txt";
      res.example_files.push_back(out_fname.str());
      std::ofstream out_file(out_fname.str().c_str());
      out_file << "+1" << feature_stream.str() << std::endl;
      out_file.close();
    }

    // NOTE: Moved this into python, since it needs to perform the attribute
    // prediction, before performing affordance prediction
    //
    // for (unsigned int a = 0; a < req.affordance_labels.size(); ++a)
    // {
    //   std::stringstream out_fname;
    //   out_fname << req.example_path << req.affordance_labels[a]
    //             << "-att-feat-test.txt";
    //   res.example_files.push_back(out_fname.str());
    //   std::ofstream out_file(out_fname.str().c_str());
    //   out_file << "+1" << feature_stream.str() << std::endl;
    //   out_file.close();
    // }

    return true;
  }

  bool extractCAFeatures(ExtractFeatures::Request &req,
                         ExtractFeatures::Response &res)
  {
    // Extract features
    std::vector<float> ab_desc;
    ab_desc.clear();
    extractBaseFeatures(frame_, ab_desc);

    // Convert to output format
    std::stringstream feature_stream;
    for (unsigned int j = 0; j < ab_desc.size(); ++j)
    {
      if (ab_desc[j] != 0.0f)
      {
        // SVM Light requires 1 based feature indexing
        feature_stream << " " << (j+1) << ":" << ab_desc[j];
      }
    }
    std::string feature_str = feature_stream.str();

    // Write files
    for (unsigned int a = 0; a < req.affordance_labels.size(); ++a)
    {
      std::stringstream out_fname;
      out_fname << req.example_path << req.affordance_labels[a] << "-test.txt";
      res.example_files.push_back(out_fname.str());
      std::ofstream out_file(out_fname.str().c_str());
      out_file << "+1" << feature_stream.str() << std::endl;
      out_file.close();
    }

    return true;
  }


  bool setupDPExample(SetupExampleFiles::Request &req,
                      SetupExampleFiles::Response &res)
  {
    std::ifstream aff_label_file(req.affordance_file.c_str());

    // Read file header information
    char header_cstr[1024];
    aff_label_file.getline(header_cstr, 1024);
    std::stringstream header_line;
    header_line << header_cstr;

    // Skip the leading comment sign
    header_line.seekg(1, std::ios_base::beg);

    // Get the number of images
    int num_imgs = 0;
    header_line >> num_imgs;

    // Get the affordance names
    while (!header_line.eof())
    {
      string aff_name;
      header_line >> aff_name;
      res.affordance_labels.push_back(aff_name);
    }

    // Exit now if we don't need to extract features
    if (!req.write_example_files)
      return true;

    // Setup output files
    std::vector<shared_ptr<std::ofstream> > out_files;
    for (unsigned int a = 0; a < res.affordance_labels.size(); ++a)
    {
      std::stringstream file_name;
      file_name << req.example_path << res.affordance_labels[a] << ".txt";
      shared_ptr<std::ofstream> out = shared_ptr<std::ofstream>(
          new std::ofstream(file_name.str().c_str()));
      out_files.push_back(out);
    }

    SlidingWindowDetector<AttributeLearningBaseFeature,
        AttributeLearningBaseFeature::Descriptor> attribute_wd;
    attribute_wd.feature_.setTextonFile(texton_file_);
    attribute_wd.feature_.setSIFTFile(sift_file_);

    // Iterate through the images, writing features and labels
    // to the output files
    for (int i = 0; i < num_imgs; ++i)
    {
      // Read the next image
      std::stringstream img_name;
      img_name << req.image_path << i << ".png";
      ROS_INFO_STREAM("Processing image: " << img_name.str());
      Mat frame;
      try
      {
        frame = cv::imread(img_name.str());
      } catch(cv::Exception e)
      {
        ROS_ERROR_STREAM(e.err);
        return false;
      }
      // Extract features from image and convert into string
      attribute_wd.scanImage(frame, std::pair<int,int>(frame.cols - 1,
                                                       frame.rows - 1), true);
      std::vector<float> ab_desc = attribute_wd.descriptors_[0];

      attribute_wd.descriptors_.clear();

      std::stringstream feature_stream;
      for (unsigned int j = 0; j < ab_desc.size(); ++j)
      {
        if (ab_desc[j] != 0.0f)
        {
          // SVM Light requires 1 based feature indexing
          feature_stream << " " << (j+1) << ":" << ab_desc[j];
        }
      }

      std::string feature_str = feature_stream.str();

      // Write labels and features to the output files
      char c_line[1024];
      aff_label_file.getline(c_line, 1024);
      std::stringstream line;
      line << c_line;
      for (unsigned int a = 0; a < res.affordance_labels.size(); ++a)
      {
        int label = 0;
        line >> label;
        if (label)
        {
          *out_files[a] << "+1";
        }
        else
        {
          *out_files[a] << "-1";
        }
        *out_files[a] << feature_str << std::endl;
      }
    }
    aff_label_file.close();

    for (unsigned int i = 0; i < out_files.size(); ++i)
    {
      out_files[i]->close();
    }

    return true;
  }

  bool setupAttExample(SetupExampleFiles::Request  &req,
                       SetupExampleFiles::Response &res)
  {
    //
    // Read in affordance stuff
    //
    std::ifstream aff_label_file(req.affordance_file.c_str());

    // Read aff file header information
    char aff_header_cstr[1024];
    aff_label_file.getline(aff_header_cstr, 1024);
    std::stringstream aff_header_line;
    aff_header_line << aff_header_cstr;

    // Skip the leading comment sign
    aff_header_line.seekg(1, std::ios_base::beg);

    // Get the number of images
    int num_imgs = 0;
    aff_header_line >> num_imgs;

    // Get the affordance names
    while (!aff_header_line.eof())
    {
      string aff_name;
      aff_header_line >> aff_name;
      res.affordance_labels.push_back(aff_name);
    }

    //
    // Read in attribute stuff
    //
    std::ifstream att_label_file(req.attribute_file.c_str());

    // Read att names header information
    char att_header_cstr[1024];
    att_label_file.getline(att_header_cstr, 1024);
    std::stringstream att_header_line;
    att_header_line << att_header_cstr;

    // Skip the leading comment sign
    att_header_line.seekg(1, std::ios_base::beg);

    // Get the attribute names
    while (!att_header_line.eof())
    {
      string att_name;
      att_header_line >> att_name;
      res.attribute_labels.push_back(att_name);
    }

    // Read att types header information
    char att_header_types_cstr[1024];
    att_label_file.getline(att_header_types_cstr, 1024);
    std::stringstream att_header_types_line;
    att_header_types_line << att_header_types_cstr;

    // Skip the leading comment sign
    att_header_types_line.seekg(1, std::ios_base::beg);
    // Get the attribute types
    for(unsigned int a = 0; a < res.attribute_labels.size(); ++a)
    {
      char att_type;
      att_header_types_line >> att_type;
      std::stringstream att_type_str;
      att_type_str << att_type;
      res.attribute_types.push_back(att_type_str.str());
    }


    // Exit now if we don't need to extract features
    if (!req.write_example_files)
    {
      aff_label_file.close();
      att_label_file.close();
      return true;
    }

    // Setup output files
    std::vector<shared_ptr<std::ofstream> > aff_out_files;
    for (unsigned int a = 0; a < res.affordance_labels.size(); ++a)
    {
      std::stringstream file_name;
      file_name << req.example_path << res.affordance_labels[a]
                << "-att-feat.txt";
      shared_ptr<std::ofstream> out = shared_ptr<std::ofstream>(
          new std::ofstream(file_name.str().c_str()));
      aff_out_files.push_back(out);
    }
    std::vector<shared_ptr<std::ofstream> > att_out_files;
    for (unsigned int a = 0; a < res.attribute_labels.size(); ++a)
    {
      std::stringstream file_name;
      file_name << req.example_path << res.attribute_labels[a] << ".txt";
      shared_ptr<std::ofstream> out = shared_ptr<std::ofstream>(
          new std::ofstream(file_name.str().c_str()));
      att_out_files.push_back(out);
    }


    SlidingWindowDetector<AttributeLearningBaseFeature,
        AttributeLearningBaseFeature::Descriptor> attribute_wd;
    attribute_wd.feature_.setTextonFile(texton_file_);
    attribute_wd.feature_.setSIFTFile(sift_file_);

    // Iterate through the images, writing features and labels
    // to the output files
    for (int i = 0; i < num_imgs; ++i)
    {
      // Read the next image
      std::stringstream img_name;
      img_name << req.image_path << i << ".png";
      ROS_INFO_STREAM("Processing image: " << img_name.str());
      Mat frame;
      try
      {
        frame = cv::imread(img_name.str());
      } catch(cv::Exception e)
      {
        ROS_ERROR_STREAM(e.err);
        aff_label_file.close();
        att_label_file.close();
        return false;
      }
      // Extract features from image and convert into string
      attribute_wd.scanImage(frame, std::pair<int,int>(frame.cols - 1,
                                                       frame.rows - 1), true);
      std::vector<float> ab_desc = attribute_wd.descriptors_[0];

      attribute_wd.descriptors_.clear();

      std::stringstream base_feature_stream;
      for (unsigned int j = 0; j < ab_desc.size(); ++j)
      {
        if (ab_desc[j] != 0.0f)
        {
          // SVM Light requires 1 based feature indexing
          base_feature_stream << " " << (j+1) << ":" << ab_desc[j];
        }
      }

      std::string base_feature_str = base_feature_stream.str();

      // Write labels and features to the attribute output files
      char att_c_line[1024];
      att_label_file.getline(att_c_line, 1024);
      std::stringstream att_line;
      att_line << att_c_line;
      std::stringstream attribute_feature_stream;
      for (unsigned int a = 0; a < res.attribute_labels.size(); ++a)
      {
        if (res.attribute_types[a] == "b")
        {
          int att_label;
          att_line >> att_label;
          if (att_label)
          {
            *att_out_files[a] << "+1";
            attribute_feature_stream << " " << (a+1) << ":" << 1;
          }
          else
          {
            *att_out_files[a] << "-1";
            attribute_feature_stream << " " << (a+1) << ":" << -1;
          }
        }
        else // Regression (float) values
        {
          float att_label;
          att_line >> att_label;
          *att_out_files[a] << att_label;
          attribute_feature_stream << " " << (a+1) << ":" << att_label;
        }
        *att_out_files[a] << base_feature_str << std::endl;
      }
      string attribute_feature_str = attribute_feature_stream.str();

      // Write attribute features to the affordance output files
      char aff_c_line[1024];
      aff_label_file.getline(aff_c_line, 1024);
      std::stringstream aff_line;
      aff_line << aff_c_line;
      for (unsigned int a = 0; a < res.affordance_labels.size(); ++a)
      {
        int aff_label = 0;
        aff_line >> aff_label;
        if (aff_label)
        {
          *aff_out_files[a] << "+1";
        }
        else
        {
          *aff_out_files[a] << "-1";
        }
        *aff_out_files[a] << attribute_feature_str << std::endl;
      }

    }
    aff_label_file.close();
    att_label_file.close();

    for (unsigned int i = 0; i < aff_out_files.size(); ++i)
    {
      aff_out_files[i]->close();
    }

    for (unsigned int i = 0; i < att_out_files.size(); ++i)
    {
      att_out_files[i]->close();
    }

    return true;
  }


  /**
   * Method to write example files for affordance classifiers conditioned on
   * object class
   *
   * @param req The service request
   * @param res The service response
   *
   * @return true if completed correctly
   */
  bool setupCAExample(SetupExampleFiles::Request &req,
                      SetupExampleFiles::Response &res)
  {
    //
    // Read in affordance stuff
    //
    std::ifstream aff_label_file(req.affordance_file.c_str());

    // Read aff file header information
    char aff_header_cstr[1024];
    aff_label_file.getline(aff_header_cstr, 1024);
    std::stringstream aff_header_line;
    aff_header_line << aff_header_cstr;

    // Skip the leading comment sign
    aff_header_line.seekg(1, std::ios_base::beg);

    // Get the number of images
    int num_imgs = 0;
    aff_header_line >> num_imgs;

    // Get the affordance names
    while (!aff_header_line.eof())
    {
      string aff_name;
      aff_header_line >> aff_name;
      res.affordance_labels.push_back(aff_name);
    }

    //
    // Read in object labels
    //
    std::ifstream obj_label_file(req.object_file.c_str());

    // Read obj file header information
    char obj_header_cstr[1024];
    obj_label_file.getline(obj_header_cstr, 1024);
    std::stringstream obj_header_line;
    obj_header_line << obj_header_cstr;

    // Skip the leading comment sign
    obj_header_line.seekg(1, std::ios_base::beg);

    // Get the number of images
    obj_header_line >> num_imgs;

    // Get the object names
    while (!obj_header_line.eof())
    {
      string obj_name;
      obj_header_line >> obj_name;
      res.object_labels.push_back(obj_name);
    }


    // Exit now if we don't need to extract features
    if (!req.write_example_files)
      return true;

    // Setup object output file
    std::stringstream file_name;
    if (req.use_ca_chain)
    {
      file_name << req.example_path << "CA-CHAIN-OBJECTS.txt";
    }
    else
    {
      file_name << req.example_path << "CA-FULL-OBJECTS.txt";
    }

    std::ofstream obj_out_file(file_name.str().c_str());

    // Setup affordance output files
    std::vector<std::vector<shared_ptr<std::ofstream> > > obj_aff_out_files;
    for (unsigned int o = 0; o < res.object_labels.size(); ++o)
    {
      std::vector<shared_ptr<std::ofstream> > aff_out_files;
      for (unsigned int a = 0; a < res.affordance_labels.size(); ++a)
      {
        std::stringstream file_name;
        file_name << req.example_path << res.object_labels[o] << '-'
                  << res.affordance_labels[a] << ".txt";
        shared_ptr<std::ofstream> out = shared_ptr<std::ofstream>(
            new std::ofstream(file_name.str().c_str()));
        aff_out_files.push_back(out);
      }
      obj_aff_out_files.push_back(aff_out_files);
    }

    // Iterate through the images, writing features and labels
    // to the output files
    for (int i = 0; i < num_imgs; ++i)
    {
      // Read the next image
      std::stringstream img_name;
      img_name << req.image_path << i << ".png";
      ROS_INFO_STREAM("Processing image: " << img_name.str());
      Mat frame;
      try
      {
        frame = cv::imread(img_name.str());
      } catch(cv::Exception e)
      {
        ROS_ERROR_STREAM(e.err);
        return false;
      }
      std::vector<float> ab_desc;
      ab_desc.clear();
      extractBaseFeatures(frame, ab_desc);

      std::stringstream base_feature_stream;
      for (unsigned int j = 0; j < ab_desc.size(); ++j)
      {
        if (ab_desc[j] != 0.0f)
        {
          // SVM Light requires 1 based feature indexing
          base_feature_stream << " " << (j+1) << ":" << ab_desc[j];
        }
      }

      std::string base_feature_str = base_feature_stream.str();

      // Write labels and features to the attribute output files
      char obj_c_line[1024];
      obj_label_file.getline(obj_c_line, 1024);
      std::stringstream obj_line;
      obj_line << obj_c_line;

      char aff_c_line[1024];
      aff_label_file.getline(aff_c_line, 1024);
      std::stringstream aff_line;
      aff_line << aff_c_line;

      int obj_label_num = -1; // This will hold the object label
      for (unsigned int o = 0; o < res.object_labels.size(); ++o)
      {
        int obj_label = 0;
        obj_line >> obj_label;

        if (obj_label)
        {
          // We set the object label here
          // SVM multiclass requires one based object labels
          obj_label_num = (o+1);
          // Write feature to the conditional affordance classifier files
          for (unsigned int a = 0; a < res.affordance_labels.size(); ++a)
          {
            int aff_label = 0;
            aff_line >> aff_label;
            if (aff_label)
            {
              *obj_aff_out_files[o][a] << "+1";
            }
            else
            {
              *obj_aff_out_files[o][a] << "-1";
            }
            *obj_aff_out_files[o][a] << base_feature_str << std::endl;
          }
        }
      }
      obj_out_file << obj_label_num << base_feature_str << std::endl;
    }

    // Cleanup files
    aff_label_file.close();
    obj_label_file.close();
    obj_out_file.close();
    for (unsigned int i = 0; i < obj_aff_out_files.size(); ++i)
    {
      for (unsigned int j = 0; j < obj_aff_out_files[i].size(); ++j)
        obj_aff_out_files[i][j]->close();
    }


    return true;
  }

  /**
   * Method to extract the base level feature from an image
   *
   * @param frame The image from which the features should be extracted
   * @param ab_desc The return parameter of the feature vector
   */
  void extractBaseFeatures(cv::Mat &frame, std::vector<float> &ab_desc)
  {
    // Extract features
    attribute_wd_.scanImage(frame, std::pair<int,int>(frame.cols - 1,
                                                      frame.rows - 1), true);
    ab_desc = attribute_wd_.descriptors_[0];


    attribute_wd_.descriptors_.clear();
  }

 protected:
  /**
   * Internal method for saving the current frame to disk.
   *
   * @param file_name The location to save the image to on disk
   *
   * @return true if the image is succesfully saved, false otherwise.
   */
  bool saveImage(string file_name, bool flip_rgb)
  {
    const std::vector<int> save_params(1, img_quality_);
    // Convert frame to correct color channel ordering
    Mat to_use;
    if (flip_rgb)
      cv::cvtColor(frame_, to_use, CV_BGR2RGB);
    else
      to_use = frame_;
    try
    {
      cv::imwrite(file_name, to_use, save_params);
      ROS_DEBUG_STREAM("Saved image " << file_name);
    }
    catch(cv::Exception e)
    {
      ROS_ERROR_STREAM(e.err);
      return false;
    }
    return true;
  }

 public:

  /**
   * Executive control function for launching the node.
   */
  void spin()
  {
    while(n_.ok())
    {
      ros::spinOnce();
    }
  }

 protected:
  ros::NodeHandle n_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  sensor_msgs::CvBridge bridge_;

  // Saving of wide and zoom images
  Mat frame_;
  string wide_save_path_;
  string zoom_save_path_;
  int wide_save_count_;
  int zoom_save_count_;
  int img_quality_;
  string texton_file_;
  string sift_file_;

  // Feature extraction stuff
  SlidingWindowDetector<AttributeLearningBaseFeature,
                        AttributeLearningBaseFeature::Descriptor> attribute_wd_;

  // Service handles
  ros::ServiceServer save_wide_;
  ros::ServiceServer save_zoom_;
  ros::ServiceServer dp_feature_extractor_;
  ros::ServiceServer att_feature_extractor_;
  ros::ServiceServer ca_feature_extractor_;
  ros::ServiceServer write_dp_example_;
  ros::ServiceServer write_att_example_;
  ros::ServiceServer write_ca_example_;
};

int main(int argc, char ** argv)
{
  ros::init(argc, argv, "attribute_classifier");
  ros::NodeHandle n;
  AttributeClassifierNode classifier_node(n);
  classifier_node.spin();
}

