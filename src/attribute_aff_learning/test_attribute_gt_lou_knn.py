#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

# import roslib; roslib.load_manifest('attribute_aff_learning')
# import rospy
import random
import k_nearest_neighbor
from knn_util import *
import matplotlib.pyplot as plotter
from math import sqrt
import numpy as np

OUT_PATH  = '/home/thermans/sandbox/svm_lists/'

AFF_TRAIN_LABELS = '/home/thermans/data/mars_lab_aff_pics/imgs-no-balls0/affordance_labels.txt'
ATT_TRAIN_LABELS = '/home/thermans/data/mars_lab_aff_pics/imgs-no-balls0/new_att_file-no-balls.txt'
AFF_TEST_LABELS = '/home/thermans/data/mars_lab_aff_pics/balls0/affordance_labels.txt'
ATT_TEST_LABELS = '/home/thermans/data/mars_lab_aff_pics/balls0/new_att_file-balls.txt'
MIN_TRAINING_SIZE = 10
TRAINING_INCREMENT = 10

_AFF_LABELS = ['PUSHABLE', 'ROLLABLE', 'GRASPABLE', 'LIFTABLE', 'DRAGABLE',
               'CARRYABLE', 'TRAVERSABLE']

def __main__():

    splits = 3
    for s in xrange(splits):
        print 'Split: ' + str(s)

        # Generate the example file from ground truth labels
        aff_labels, num_train_images, att_types = build_test_train_files(s)
        max_training_size = num_train_images

        test_file = OUT_PATH + _AFF_LABELS[0] + '-' + str(s) + '-test.txt'
        test_examples = read_example_file_att_features(test_file, att_types)

        for train_size in range(MIN_TRAINING_SIZE, max_training_size+1,
                                TRAINING_INCREMENT):
            print 'Training size is: ' + str(train_size) + '\n'
            aff_values = []

            for i, a in enumerate(_AFF_LABELS):
                train_file = OUT_PATH+str(a)+'-'+str(s)+'-train-'+str(train_size)+'.txt'
                label_values = read_example_file_labels(train_file)
                aff_values.append(label_values)
            att_features = read_example_file_att_features(
                train_file, att_types)

            knn = k_nearest_neighbor.KNearestNeighbors(5)
            knn.dist_func = knn.split_att_metric
            knn.exemplars = np.asarray(att_features)
            knn.labels = np.asarray(aff_values).T
            knn.feat_types = np.asarray(att_types)
            knn.estimate_binary_float_channel_weights()

            # Perform the testing
            aff_predictions = knn.classify(test_examples)

            for i, a in enumerate(_AFF_LABELS):
                out_file = OUT_PATH+a+'-'+str(s)+'-results-'+str(train_size)+'.txt'
                write_output_file(out_file, aff_predictions[:,i])

    # Analyze output
    affordance_prediction_vs_training_size(OUT_PATH, splits,
                                           MIN_TRAINING_SIZE, max_training_size,
                                           TRAINING_INCREMENT,
                                           title_prefix='Ground Truth Attribute ')

def build_test_train_files(s):
    raw_aff_train_data = read_affordance_file(AFF_TRAIN_LABELS)
    raw_att_train_data = read_new_attribute_file(ATT_TRAIN_LABELS)

    raw_aff_test_data = read_affordance_file(AFF_TEST_LABELS)
    raw_att_test_data = read_new_attribute_file(ATT_TEST_LABELS)

    #
    # Read the base data
    #

    # Training data
    num_train_images = int(raw_aff_train_data[0])
    aff_labels = raw_aff_train_data[1]
    aff_train_values = raw_aff_train_data[2]
    att_train_labels = raw_att_train_data[0]

    att_types = raw_att_test_data[1]
    att_train_values = raw_att_train_data[2]
    aff_train_feature_data = []

    # Testing data
    num_test_images = int(raw_aff_test_data[0])
    aff_test_values = raw_aff_test_data[2]
    att_test_values = raw_att_test_data[2]
    aff_test_feature_data = []

    #
    # Transform attribute labels into feature vectors
    #
    for i in xrange(num_train_images):
        att_feature_str = ''
        for j, att in enumerate(att_train_values[i]):
            att_feature_str += ' ' + str(j+1) + ':'
            a_val = float(att)
            if att_types[j] == 'b':
                if a_val == 1:
                    att_feature_str += '1'
                else:
                    att_feature_str += '-1'
            else:
                att_feature_str += str(a_val)
        att_feature_str.rstrip()
        aff_train_feature_data.append(att_feature_str)

    for i in xrange(num_test_images):
        att_feature_str = ''
        for j, att in enumerate(att_test_values[i]):
            att_feature_str += ' ' + str(j+1) + ':'
            a_val = float(att)
            if att_types[j] == 'b':
                if a_val == 1:
                    att_feature_str += '1'
                else:
                    att_feature_str += '-1'
            else:
                att_feature_str += str(a_val)
        att_feature_str.rstrip()
        aff_test_feature_data.append(att_feature_str)

    aff_train_example_data = []
    for j, a in enumerate(aff_labels):
        aff_train_example_data.append([])
        for i in xrange(num_train_images):
            gt = int(aff_train_values[i][j])
            if gt == 1:
                out_line = '+1'
            else:
                out_line = '-1'
            out_line += aff_train_feature_data[i] + '\n'
            aff_train_example_data[j].append(out_line)

    aff_test_example_data = []
    for j, a in enumerate(aff_labels):
        aff_test_example_data.append([])
        for i in xrange(num_test_images):
            gt = int(aff_test_values[i][j])
            if gt == 1:
                out_line = '+1'
            else:
                out_line = '-1'
            out_line += aff_test_feature_data[i] + '\n'
            aff_test_example_data[j].append(out_line)

    aff_test_data = aff_test_values
    aff_test_feature_data = aff_test_example_data

    for j, a in enumerate(aff_labels):
        aff_file = OUT_PATH + a + '-' + str(s) + '-test.txt'
        write_example_file(aff_file, aff_test_feature_data[j])

    # Create training files of different sizes
    min_training_size = MIN_TRAINING_SIZE
    max_training_size = num_train_images

    available_indices = range(0,num_train_images)

    # TODO: This is bad for object class stuff, need to randomize it
    for train_size in range(min_training_size, max_training_size+1,
                            TRAINING_INCREMENT):
        aff_train_data = []
        aff_train_feature_data = []

        train_avail_indices = available_indices[:]
        for a in aff_labels:
            aff_train_feature_data.append([])

        for j in xrange(train_size):
            idx = train_avail_indices[random.randint(0,len(train_avail_indices)-1)]
            try:
                train_avail_indices.remove(idx)
            except ValueError:
                print 'Issue removing value of: ' + str(idx) + ' from list'

            aff_train_data.append(aff_train_values[idx])
            for k, a in enumerate(aff_labels):
                aff_train_feature_data[k].append(aff_train_example_data[k][idx])

        for j, a in enumerate(aff_labels):
            aff_file = OUT_PATH + a + '-' + str(s) + '-train-' + str(train_size) + '.txt'
            write_example_file(aff_file, aff_train_feature_data[j])

    return aff_labels, num_train_images, att_types

def sign(x):
    if x < 0:
        return -1
    if x > 0:
        return 1
    return 0


def affordance_prediction_vs_training_size(base_name, splits, min_train_size,
                                           max_train_size, train_inc,
                                           name_suffix='', title_prefix=''):
    train_sizes = range(min_train_size, max_train_size+1, train_inc)
    plotter.figure()
    plotted_lines = []
    for i, a in enumerate(_AFF_LABELS):
        curves = []
        test_errors = []
        test_error_values = []
        for j in xrange(len(train_sizes)):
            test_errors.append(0.0)
            test_error_values.append([])
        for s in xrange(splits):
            curve = []
            for j, t in enumerate(train_sizes):
                cur_name = base_name + a
                aff_gt_file = file(cur_name + '-' + str(s) +'-' 'test.txt', 'r')
                aff_pred_file = file(cur_name + '-' + str(s) + '-results-' +
                                     str(t) + '.txt', 'r')

                # Parse ground truth and predicted labels from files
                pred_labels = [sign(float(d.strip().split()[0])) \
                                   for d in aff_pred_file.readlines()]
                aff_pred_file.close()

                gt_labels = [sign(float(d.strip().split()[0])) \
                                 for d in aff_gt_file.readlines()]
                aff_gt_file.close()

                # Store values to calculate things
                tp = 0.0
                tn = 0.0
                fp = 0.0
                fn = 0.0
                for out, truth in zip(pred_labels, gt_labels):
                    if truth == out:
                        if out > 0:
                            tp += 1.0
                        else:
                            tn += 1.0
                    else:
                        if out > 0:
                            fp += 1.0
                        else:
                            fn += 1.0
                curve.append((tp,tn,fp,fn))
                cur_error = ((fp+fn)/(tp+tn+fp+fn))
                test_errors[j] += cur_error
                test_error_values[j].append(cur_error)
            curves.append(curve)

        test_error_error_bars = []
        for j in xrange(len(train_sizes)):
            test_errors[j] /= float(splits)
            x_bar = test_errors[j]
            cur_vals = test_error_values[j]
            std_dev = 0
            for x in cur_vals:
                std_dev += (x - x_bar)**2.0
            std_dev = sqrt(std_dev/float(splits))
            se = std_dev / sqrt(float(splits))
            test_error_error_bars.append(se)

        print 'Plotting:', a
        #plotter.figure()
        #plotter.title(a)
        plotted_lines.append(plotter.errorbar(train_sizes, test_errors,
                                              yerr=test_error_error_bars)[0])

    plotter.xlabel('Training Size')
    plotter.ylabel('Test Error')
    plotter.xlim(train_sizes[0], train_sizes[-1])
    ymin, ymax = plotter.ylim()
    plotter.ylim((0.0, ymax))
    plotter.title(title_prefix + 'Affordance Prediction Results')
    aff_title_labels = [a.title() for a in _AFF_LABELS]
    plotter.figlegend(plotted_lines, aff_title_labels, 'upper right',
                      numpoints=3, markerscale=0.6, ncol=2)
    plotter.draw()
    plotter.show()
    return

if __name__ == '__main__':
    __main__()
