#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import roslib; roslib.load_manifest('attribute_aff_learning')
import rospy
import random
import time
from attribute_aff_learning.srv import *
from svm_util import *

def aff_train(write_examples, use_dp, use_ca_full, use_ca_chain):
    srv_call = AffordanceTrainRequest()
    srv_call.write_example_files = write_examples
    srv_call.train_model_files = True

    res = None
    if use_dp:
        srv_call.use_dp = True
        srv_call.use_ca_full = False
        srv_call.use_ca_chain = False
        srv_call.use_att = False
        dp_svm = rospy.ServiceProxy('dp_aff_train', AffordanceTrain)
        res = dp_svm(srv_call)
    elif use_ca_full:
        srv_call.use_dp = False
        srv_call.use_ca_full = True
        srv_call.use_ca_chain = False
        srv_call.use_att = False
        ca_full_svm = rospy.ServiceProxy('ca_full_aff_train', AffordanceTrain)
        res = ca_full_svm(srv_call)
    elif use_ca_chain:
        srv_call.use_dp = False
        srv_call.use_ca_full = False
        srv_call.use_ca_chain = True
        srv_call.use_att = False
        ca_chain_svm = rospy.ServiceProxy('ca_chain_aff_train', AffordanceTrain)
        res = ca_chain_svm(srv_call)
    else:
        srv_call.use_dp = False
        srv_call.use_ca_full = False
        srv_call.use_ca_chain = False
        srv_call.use_att = True
        att_svm = rospy.ServiceProxy('attribute_aff_train', AffordanceTrain)
        res = att_svm(srv_call)

    return res

def aff_test(affordance_labels, attribute_labels, att_types, object_labels,
             use_active, use_dp, use_ca_full, use_ca_chain):

    srv_call = AffordanceClassifyRequest()
    srv_call.affordance_labels = affordance_labels
    srv_call.attribute_labels = attribute_labels
    srv_call.attribute_types = att_types
    srv_call.object_labels = object_labels
    srv_call.use_current_image = use_active

    res = None
    if use_dp:
        srv_call.use_dp = True
        srv_call.use_ca_full = False
        srv_call.use_ca_chain = False
        srv_call.use_att = False
        dp_svm = rospy.ServiceProxy('dp_aff_test', AffordanceClassify)
        res = dp_svm(srv_call)
    elif use_ca_full:
        srv_call.use_dp = False
        srv_call.use_ca_full = True
        srv_call.use_ca_chain = False
        srv_call.use_att = False
        ca_full_svm = rospy.ServiceProxy('ca_full_aff_test', AffordanceClassify)
        res = ca_full_svm(srv_call)
    elif use_ca_chain:
        srv_call.use_dp = False
        srv_call.use_ca_full = False
        srv_call.use_ca_chain = True
        srv_call.use_att = False
        ca_chain_svm = rospy.ServiceProxy('ca_chain_aff_test', AffordanceClassify)
        res = ca_chain_svm(srv_call)
    else:
        srv_call.use_dp = False
        srv_call.use_ca_full = False
        srv_call.use_ca_chain = False
        srv_call.use_att = True
        att_svm = rospy.ServiceProxy('attribute_aff_test', AffordanceClassify)
        res = att_svm(srv_call)

    return res

def extract_att_features(image_path, example_path, aff_name, att_name):
    # Setup the service
    example_file_writer = rospy.ServiceProxy("attribute_example_writer",
                                             SetupExampleFiles)

    # Setup the request to perform the feature extraction
    write_request = SetupExampleFilesRequest()
    write_request.image_path = image_path
    write_request.example_path = example_path
    write_request.affordance_file = aff_name
    write_request.attribute_file = att_name
    write_request.write_example_files = True

    # Perform the feature extraction
    example_file_writer(write_request)

def extract_dp_features(image_path, example_path, aff_name):
    # Setup the service
    example_file_writer = rospy.ServiceProxy("dp_example_writer",
                                             SetupExampleFiles)

    # Setup the request to perform the feature extraction
    write_request = SetupExampleFilesRequest()
    write_request.image_path = image_path
    write_request.example_path = example_path
    write_request.affordance_file = aff_name
    write_request.write_example_files = True

    # Perform the feature extraction
    example_file_writer(write_request)

def extract_ca_features(image_path, example_path, aff_name, obj_name,
                        use_ca_chain):
    # Setup the service
    example_file_writer = rospy.ServiceProxy("ca_example_writer",
                                             SetupExampleFiles)

    # Setup the request to perform the feature extraction
    write_request = SetupExampleFilesRequest()
    write_request.image_path = image_path
    write_request.example_path = example_path
    write_request.affordance_file = aff_name
    write_request.object_file = obj_name
    write_request.write_example_files = True
    write_request.use_ca_chain = use_ca_chain

    # Perform the feature extraction
    example_file_writer(write_request)

def write_ca_example_files(obj_file_name,  obj_file_data, obj_labels,
                           aff_labels, base_name, aff_files_data_raw):
    '''
    Decompose the affordance data into the correct components for writing the
    CA full example files
    '''
    # This file is for the object classification
    write_example_file(obj_file_name, obj_file_data)

    aff_files_data = []
    for data in aff_files_data_raw:
        aff_files_data.append(data[:])

    obj_aff_files = []

    # Open each of the o*a files
    for o in obj_labels:
        a_files = []
        for a in aff_labels:
            file_name = base_name + o + '-' + a + '.txt'
            data_out = file(file_name, 'w')
            a_files.append(data_out)
        obj_aff_files.append(a_files)

    # Write the data one line at a time to the appropriate place
    for d in obj_file_data:
        obj_index = int(d[0])-1
        obj_name = obj_labels[obj_index]

        for k, aff in enumerate(aff_labels):
            to_write = aff_files_data[k][0]
            aff_files_data[k].remove(to_write)
            obj_aff_files[obj_index][k].write(to_write)

    # Cleanup files
    for i, o in enumerate(obj_labels):
        for j, a in enumerate(aff_labels):
            obj_aff_files[i][j].close()


if __name__ == '__main__':
    rospy.init_node('offline_affordance_svm_node')

    #
    # Set learning parameters
    #
    use_dp = False
    use_ca_full = True
    use_ca_chain = False
    use_att = not (use_dp or use_ca_full or use_ca_chain)
    use_active = False
    extract_base_files = False
    # NOTE: keep this false, we extract all at start, setting it to true writes
    # the files within the service call.
    write_train_files = False

    if use_dp:
        rospy.loginfo('Using direct perception!')
    if use_ca_full:
        rospy.loginfo('Using ca full!')
    if use_ca_chain:
        rospy.loginfo('Using ca chain!')
    if use_att:
        rospy.loginfo('Using attribute based!')

    #
    # Get parameters of file locations
    #
    base_affordance_name = rospy.get_param('base_affordance_file')

    base_object_name = rospy.get_param('base_ca_full_file')
    base_image_path = rospy.get_param('base_image_path')
    base_example_path = rospy.get_param('base_example_path')
    use_new_attributes = rospy.get_param('use_new_att_file')
    if use_new_attributes:
        base_attribute_name = rospy.get_param('base_new_attribute_file')
    else:
        base_attribute_name = rospy.get_param('base_attribute_file')

    #
    # Read the base data
    #
    aff_data = read_affordance_file(base_affordance_name)
    num_images = int(aff_data[0])
    aff_labels = aff_data[1]
    aff_values = aff_data[2]

    if use_new_attributes:
        att_data = read_new_attribute_file(base_attribute_name)
        att_labels = att_data[0]
        att_types = att_data[1]
        att_values = att_data[2]
    else:
        att_data = read_new_attribute_file(base_attribute_name)
        att_labels = att_data[0]
        att_values = att_data[1]
        att_types = []

    obj_data = read_object_file(base_object_name)
    obj_labels = obj_data[0]
    obj_values = obj_data[1]

    image_loc_file = base_image_path + 'image_locs.txt'
    image_locs = read_image_loc_file(image_loc_file)

    #
    # Extract all features and write each set to master files
    #
    if extract_base_files:
        rospy.loginfo('Extracting features from all ' + str(num_images) +
                      ' images')
        if use_dp:
            extract_dp_features(base_image_path, base_example_path,
                                base_affordance_name)
        elif use_ca_full or use_ca_chain:
            extract_ca_features(base_image_path, base_example_path,
                                base_affordance_name, base_object_name,
                                use_ca_chain)
        else:
            extract_att_features(base_image_path, base_example_path,
                                 base_affordance_name, base_attribute_name)

    #
    # Read extracted features into python for sorting and saving purposes
    #
    aff_feature_data = []
    att_feature_data = []
    obj_feature_data = []
    aft_test_pos_neg_dists = []
    for a in aff_labels:
        aft_test_pos_neg_dists.append([])
        if use_dp:
            aff_file = base_example_path + a + '.txt'
            aff_feature_data.append(read_example_file(aff_file))
        elif use_ca_full or use_ca_chain:
            # NOTE: This assumes objects are all grouped when initially read in
            aff_file = []
            for o in obj_labels:
                ca_aff_file = base_example_path + o +'-' + a + '.txt'
                aff_file.extend(read_example_file(ca_aff_file))
            aff_feature_data.append(aff_file)

        elif use_att:
            aff_file = base_example_path + a + '-att-feat.txt'
            aff_feature_data.append(read_example_file(aff_file))

    if use_ca_full:
        # Store the raw data for object classification
        ca_full_file = base_example_path + 'CA-FULL-OBJECTS.txt'
        obj_feature_data = read_example_file(ca_full_file)
    elif use_ca_chain:
        # Store the raw data for object classification
        ca_full_file = base_example_path + 'CA-CHAIN-OBJECTS.txt'
        obj_feature_data = read_example_file(ca_full_file)

    elif use_att:
        for a in att_labels:
            att_file = base_example_path + a + '.txt'
            att_feature_data.append(read_example_file(att_file))

    #
    # generate training and testing splits
    #
    seed_val = time.time()
    random.seed(seed_val)
    rospy.loginfo('Seeding with value of: ' + str(seed_val))
    test_percent = 1.0 - rospy.get_param('max_train_percent')
    testing_size = int(test_percent*num_images)
    min_training_size = rospy.get_param('min_train_size')
    max_training_size = num_images - testing_size

    num_splits = rospy.get_param('num_data_partitions')
    for i in xrange(num_splits):
        rospy.loginfo('Splliting data for set: ' + str(i))

        #
        # Extract testing data
        #
        att_test_data = []
        aff_test_data = []
        obj_test_data = []

        att_test_feature_data = []
        aff_test_feature_data = []
        obj_test_feature_data = [] # Object class data
        obj_test_aff_feature_data = [] # Object affordance data

        # Setup nested lists for holding data
        for a in aff_labels:
            aff_test_feature_data.append([])
        if use_att:
            for a in att_labels:
                att_test_feature_data.append([])
        if use_ca_full or use_ca_chain:
            obj_test_feature_data = []

        available_indices = range(0,num_images)
        test_indices = []
        test_image_locs = []
        rospy.loginfo('Testing size:' + str(testing_size) + '\n')
        for j in xrange(testing_size):
            val = available_indices[random.randint(0,len(available_indices)-1)]
            test_image_locs.append(image_locs[val])
            try:
                available_indices.remove(val)
            except ValueError:
                rospy.logerror('Issue removing value of: ' + str(val) +
                               ' from list')

            # Extract the correct labels
            aff_test_data.append(aff_values[val])
            if use_ca_full or use_ca_chain:
                obj_test_data.append(obj_values[val])
            if use_att:
                att_test_data.append(att_values[val])

            # Get the data
            for k, a in enumerate(aff_labels):
                aff_test_feature_data[k].append(aff_feature_data[k][val])
            if use_att:
                for k, a in enumerate(att_labels):
                    att_test_feature_data[k].append(att_feature_data[k][val])
            if use_ca_full or use_ca_chain:
                obj_test_feature_data.append(obj_feature_data[val])

        # Vary the training size
        rospy.loginfo('Training on set: ' + str(i))
        train_step = int(rospy.get_param('train_increment'))
        for train_size in range(min_training_size, max_training_size+1,
                                train_step):
            rospy.loginfo('Training size:' + str(train_size))
            #
            # Remainder is used as training data
            #
            att_train_data = []
            aff_train_data = []
            obj_train_data = []

            att_train_feature_data = []
            aff_train_feature_data = []

            obj_train_feature_data = [] # Object class data
            obj_train_aff_feature_data = [] # Object affordance data

            for a in aff_labels:
                aff_train_feature_data.append([])
            if use_att:
                for a in att_labels:
                    att_train_feature_data.append([])
            if use_ca_full or use_ca_chain:
                obj_train_feature_data = []

            train_avail_indices = available_indices[:]
            train_indices = []
            train_image_locs = []

            #
            # Setup file names for split and train_size
            #

            # These files hold the labels split into testing and training
            att_train_name = base_attribute_name[:-4] + '-train-' + str(i) + \
                '-' + str(train_size) + '.txt'
            att_test_name = base_attribute_name[:-4] + '-test-' + str(i) + \
                '-' + str(train_size) + '.txt'
            obj_train_name = base_object_name[:-4] + '-train-' + str(i) + \
                '-' + str(train_size) + '-ca-full.txt'
            obj_test_name = base_object_name[:-4] + '-test-' + str(i) + \
                '-' + str(train_size) + '-ca-full.txt'
            aff_test_name = ''
            aff_train_name = ''
            if use_dp:
                aff_test_name = base_affordance_name[:-4] + '-test-' + \
                    str(i) + '-' + str(train_size) + '.txt'
                aff_train_name = base_affordance_name[:-4] + '-train-' + \
                    str(i) + '-' + str(train_size) + '.txt'
            elif use_ca_full:
                aff_test_name = base_affordance_name[:-4] + '-test-' + \
                    str(i) + '-' + str(train_size) + '-ca-full.txt'
                aff_train_name = base_affordance_name[:-4] + '-train-' + \
                    str(i) + '-' + str(train_size) + '-ca-full.txt'
            elif use_ca_chain:
                aff_test_name = base_affordance_name[:-4] + '-test-' + \
                    str(i) + '-' + str(train_size) + '-ca-chain.txt'
                aff_train_name = base_affordance_name[:-4] + '-train-' + \
                    str(i) + '-' + str(train_size) + '-ca-chain.txt'
            else:
                aff_test_name = base_affordance_name[:-4] + '-test-' + \
                    str(i) + '-' + str(train_size) + '-att-feat.txt'
                aff_train_name = base_affordance_name[:-4] + '-train-' + \
                    str(i) + '-' + str(train_size) + '-att-feat.txt'

            # These are the base file names for the various example files
            test_example_name = base_example_path + 'test-' + str(i) + '-' + \
                str(train_size) + '-'
            train_example_name = base_example_path + 'train-' + str(i) + '-' + \
                str(train_size) + '-'

            for j in xrange(train_size):
                # TODO: Do something smarter to preserve object class diversity
                idx = train_avail_indices[random.randint(
                        0,len(train_avail_indices)-1)]
                try:
                    train_avail_indices.remove(idx)
                except ValueError:
                    print 'Issue removing value of: ' + str(idx) + ' from list'

                # Append data to the training set
                aff_train_data.append(aff_values[idx])
                train_indices.append(idx)
                train_image_locs.append(image_locs[idx])

                if use_ca_full or use_ca_chain:
                    obj_train_data.append(obj_values[idx])
                if use_att:
                    att_train_data.append(att_values[idx])

                for k, a in enumerate(aff_labels):
                    aff_train_feature_data[k].append(aff_feature_data[k][idx])
                if  use_att:
                    for k, a in enumerate(att_labels):
                        att_train_feature_data[k].append(att_feature_data[k][idx])
                if use_ca_full or use_ca_chain:
                    obj_train_feature_data.append(obj_feature_data[idx])

            # Write the training files
            write_affordance_file(aff_train_name, len(aff_train_data),
                                  aff_labels, aff_train_data)
            if use_att:
                if use_new_attributes:
                    write_new_attribute_file(att_train_name, att_labels,
                                             att_types, att_train_data)
                else:
                    write_attribute_file(att_train_name, att_labels,
                                         att_train_data)
            if use_ca_full or use_ca_chain:
                write_object_file(obj_train_name, len(aff_train_data),
                                  obj_labels, obj_train_data)

            for j, a in enumerate(aff_labels):
                aff_file = ''
                if use_dp:
                    aff_file = train_example_name + a + '.txt'
                    write_example_file(aff_file, aff_train_feature_data[j])
                elif use_ca_full or use_ca_chain:
                    pass
                else:
                    aff_file = train_example_name + a + '-att-feat.txt'
                    write_example_file(aff_file, aff_train_feature_data[j])

            if use_att:
                for j, a in enumerate(att_labels):
                    att_file = train_example_name + a + '.txt'
                    write_example_file(att_file, att_train_feature_data[j])
            if use_ca_full:
                obj_file = train_example_name + 'CA-FULL-OBJECTS.txt'
                write_ca_example_files(obj_file, obj_train_feature_data,
                                       obj_labels, aff_labels,
                                       train_example_name,
                                       aff_train_feature_data)
            elif use_ca_chain:
                obj_file = train_example_name + 'CA-CHAIN-OBJECTS.txt'
                write_ca_example_files(obj_file, obj_train_feature_data,
                                       obj_labels, aff_labels,
                                       train_example_name,
                                       aff_train_feature_data)

            # Record which images were used for training
            train_image_loc_name = train_example_name + 'image_locs.txt'
            write_image_loc_file(train_image_loc_name, train_image_locs)

            #
            # Write test files to disk
            #
            for j, a in enumerate(aff_labels):
                aff_file = ''
                if use_dp:
                    aff_file = test_example_name + a + '.txt'
                    write_example_file(aff_file, aff_test_feature_data[j])
                elif use_ca_full or use_ca_chain:
                    pass
                elif use_att:
                    aff_file = test_example_name + a + '-att-feat.txt'
                    write_example_file(aff_file, aff_test_feature_data[j])

            if use_att:
                for j, a in enumerate(att_labels):
                    att_file = test_example_name + a + '.txt'
                    write_example_file(att_file, att_test_feature_data[j])

            # Writ the file holding the object class data
            if use_ca_full:
                obj_file = test_example_name + 'CA-FULL-OBJECTS.txt'
                write_ca_example_files(obj_file, obj_test_feature_data,
                                       obj_labels, aff_labels,
                                       test_example_name,
                                       aff_test_feature_data)
            elif use_ca_chain:
                obj_file = test_example_name + 'CA-CHAIN-OBJECTS.txt'
                write_ca_example_files(obj_file, obj_test_feature_data,
                                       obj_labels, aff_labels,
                                       test_example_name,
                                       aff_test_feature_data)

            #
            # Write label files of testing data
            #
            write_affordance_file(aff_test_name, len(aff_test_data), aff_labels,
                                  aff_test_data)
            if use_att:
                if use_new_attributes:
                    write_new_attribute_file(att_test_name, att_labels,
                                             att_types, att_test_data)
                else:
                    write_attribute_file(att_test_name, att_labels,
                                         att_test_data)
            if use_ca_full or use_ca_chain:
                write_object_file(obj_test_name, len(aff_test_data), obj_labels,
                                  obj_test_data)

            # Write the images being used for testing
            test_image_loc_name = test_example_name + 'image_locs.txt'
            write_image_loc_file(test_image_loc_name, test_image_locs)

            #
            # Set parameter values for this training set
            #
            rospy.set_param('train_example_path', train_example_name)
            rospy.set_param('train_affordance_file', aff_train_name)
            rospy.set_param('train_attribute_file', att_train_name)
            rospy.set_param('train_ca_full_file', obj_train_name)
            rospy.set_param('train_ca_chain_file', obj_train_name)

            rospy.set_param('test_affordance_file', aff_test_name)
            rospy.set_param('test_attribute_file', att_test_name)
            rospy.set_param('test_ca_full_file', obj_test_name)
            rospy.set_param('test_ca_chain_file', obj_test_name)
            rospy.set_param('test_example_path', test_example_name)

            #
            # Perform training
            #
            rospy.loginfo('Performing training')
            train_response = aff_train(write_train_files, use_dp, use_ca_full,
                                       use_ca_chain)

            test_affordance_labels = train_response.affordance_labels
            test_attribute_labels = train_response.attribute_labels
            test_attribute_types = train_response.attribute_types
            test_object_labels = train_response.object_labels

            #
            # Perform testing
            #
            rospy.loginfo('Testing on set ' +  str(i) + '\n')
            test_response = aff_test(test_affordance_labels,
                                     test_attribute_labels,
                                     test_attribute_types,
                                     test_object_labels, use_active, use_dp,
                                     use_ca_full, use_ca_chain)
