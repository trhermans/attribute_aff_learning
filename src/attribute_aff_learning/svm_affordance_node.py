#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import roslib; roslib.load_manifest('attribute_aff_learning')
import rospy
from svm_light_wrapper import SVMLight
from majority_vote_classifier import *
from attribute_aff_learning.srv import *
import svm_util

def sign(x):
    if x < 0:
        return -1
    if x > 0:
        return 1
    return 0

class SVMAffordanceNode:
    '''
    ROS Node that orchestrates the calling of all SVMs required to perform
    attribute based affordance classification.
    '''

    def run(self):
        '''
        Method initializes the node and spins the classifier listeners
        '''
        rospy.init_node('svm_affordance_node',log_level=rospy.DEBUG)

        # DP stuff
        self.dp_example_file_writer = rospy.ServiceProxy("dp_example_writer",
                                                         SetupExampleFiles)
        self.dp_feature_extractor = rospy.ServiceProxy('dp_feature_extractor',
                                                       ExtractFeatures)
        self.dp_train_service = rospy.Service('dp_aff_train', AffordanceTrain,
                                              self.handle_aff_training)
        self.dp_classify_service = rospy.Service('dp_aff_test',
                                                 AffordanceClassify,
                                                 self.handle_aff_testing)

        # CA stuff
        self.ca_example_file_writer = rospy.ServiceProxy('ca_example_writer',
                                                         SetupExampleFiles)
        self.ca_feature_extractor = rospy.ServiceProxy('ca_feature_extractor',
                                                       ExtractFeatures)
        # CA full stuff
        self.ca_full_train_service = rospy.Service('ca_full_aff_train',
                                                   AffordanceTrain,
                                                   self.handle_aff_training)
        self.ca_full_classify_service = rospy.Service('ca_full_aff_test',
                                                      AffordanceClassify,
                                                      self.handle_aff_testing)
        # CA chain stuff
        self.ca_chain_train_service = rospy.Service('ca_chain_aff_train',
                                                    AffordanceTrain,
                                                    self.handle_aff_training)
        self.ca_chain_classify_service = rospy.Service('ca_chain_aff_test',
                                                       AffordanceClassify,
                                                       self.handle_aff_testing)
        self.ca_chain_classifiers = []
        # Attribute stuff
        self.att_example_file_writer = rospy.ServiceProxy(
            "attribute_example_writer", SetupExampleFiles)
        self.att_feature_extractor = rospy.ServiceProxy(
            'attribute_feature_extractor', ExtractFeatures)
        self.att_train_service = rospy.Service('attribute_aff_train',
                                               AffordanceTrain,
                                               self.handle_aff_training)
        self.att_classify_service = rospy.Service('attribute_aff_test',
                                                  AffordanceClassify,
                                                  self.handle_aff_testing)
        # Detach process
        rospy.spin()

    def handle_aff_training(self, request):
        '''
        Callback method for the training service.
        Extracts features from the dataset.
        Builds a classifier for each affordance.
        '''

        self.train_image_path = rospy.get_param('train_image_path')
        self.train_example_path = rospy.get_param('train_example_path')
        self.train_affordance_file = rospy.get_param('train_affordance_file')
        self.train_attribute_file = rospy.get_param('train_attribute_file')
        if request.use_ca_full:
            self.train_ca_file = rospy.get_param('train_ca_full_file')
        elif request.use_ca_chain:
            self.train_ca_file = rospy.get_param('train_ca_chain_file')

        write_request = SetupExampleFilesRequest()
        write_request.image_path = self.train_image_path
        write_request.example_path = self.train_example_path
        write_request.affordance_file = self.train_affordance_file
        write_request.attribute_file = self.train_attribute_file
        if request.use_ca_full or request.use_ca_chain:
            write_request.object_file = self.train_ca_file
        write_request.write_example_files = request.write_example_files
        rospy.logdebug('Calling write service')

        write_response = None
        if request.use_dp:
            write_response = self.dp_example_file_writer(write_request)
        elif request.use_ca_full or request.use_ca_chain:
            write_response = self.ca_example_file_writer(write_request)
        else:
            write_response = self.att_example_file_writer(write_request)

        rospy.loginfo('Finished writing example file')
        response = AffordanceTrainResponse()

        # Separate method for ca to make this one clean
        if request.use_ca_full or request.use_ca_chain:
            rospy.logdebug('Calling handle_ca_aff_training')
            response = self.handle_ca_aff_training(write_response, request)
            rospy.logdebug('Called handle_ca_aff_training')
            return response

        # Perform training for each attribute
        for a_i, a in enumerate(write_response.attribute_labels):
            if not request.use_att:
                break
            rospy.loginfo('Training for attribute ' + a)
            example_file_a = self.train_example_path + a + '.txt'
            model_file_a = self.train_example_path + a + '-model.txt'

            # Run SVM
            rospy.loginfo("Training model files")
            svm_a = SVMLight()
            svm_a.train_file = example_file_a
            svm_a.model_file = model_file_a
            svm_a.kernel_type = 4 # Use multichannel chi-square kernel
            svm_a.u_value = 'mc-chi'
            if write_response.attribute_types[a_i] == 'f':
                # Set to regression if the attribute type is float
                svm_a.use_regression = True
            else:
                label_values = svm_util.read_example_file_labels(example_file_a)
                num_neg_examples = sum([lv == '-1' for lv in label_values])
                num_pos_examples = len(label_values) - num_neg_examples
                if num_neg_examples > 0 and num_pos_examples > 0:
                    svm_a.j_value = ((num_neg_examples+1) /
                                     float(num_pos_examples+1))
                    rospy.loginfo('Using J value of ' + str(svm_a.j_value))
            svm_a.learn()

            rospy.loginfo("Writing responses")
            response.attribute_labels.append(a)
            response.attribute_types.append(write_response.attribute_types[a_i])
            response.model_file_names.append(model_file_a)

        # Perform training for each affordance
        for a in write_response.affordance_labels:
            rospy.loginfo('Training for affordance ' + a)
            example_file_a = ''
            model_file_a = ''
            if request.use_dp:
                example_file_a = self.train_example_path + a + '.txt'
                model_file_a = self.train_example_path + a + '-model.txt'
            else:
                example_file_a = self.train_example_path + a + '-att-feat.txt'
                model_file_a = self.train_example_path + a + '-att-feat-model.txt'

            # Run SVM
            svm_a = SVMLight()
            svm_a.train_file = example_file_a
            svm_a.model_file = model_file_a

            if request.use_dp:
                svm_a.kernel_type = 4 # Use chi-square kernel
                svm_a.u_value = 'mc-chi'
            else:
                # svm_a.kernel_type = 0 # Use a linear kernel
                # svm_a.kernel_type = 1 # Use a polynomial kernel
                # svm_a.kernel_type = 2 # Use an RBF kernel
                # svm_a.g_value = 0.01 # Gamma parameter for RBF
                # svm_a.kernel_type = 3 # Use a sigmoid kernel
                svm_a.kernel_type = 4 # Use custom kernel
                svm_a.u_value = 'mc-bin-float'
            label_values = svm_util.read_example_file_labels(example_file_a)
            num_neg_examples = sum([lv == '-1' for lv in label_values])
            num_pos_examples = len(label_values) - num_neg_examples

            if num_neg_examples > 0 and num_pos_examples > 0:
                svm_a.j_value = (num_neg_examples) / float(num_pos_examples)
                rospy.loginfo('Using J value of ' + str(svm_a.j_value))
            svm_a.learn()

            response.affordance_labels.append(a)
            response.model_file_names.append(model_file_a)

        return response

    def handle_ca_aff_training(self, write_response, request):
        '''
        Performs training of the object class classifiers as well as the
        affordance classifiers conditioned on those object classes.
        '''
        response = AffordanceTrainResponse()

        # Train multiclass classifier for object classes
        if request.use_ca_full:
            example_file_o = self.train_example_path + 'CA-FULL-OBJECTS.txt'
            model_file_o = self.train_example_path + 'CA-FULL-OBJECTS-model.txt'
        else:
            example_file_o = self.train_example_path + 'CA-CHAIN-OBJECTS.txt'
            model_file_o = self.train_example_path + 'CA-CHAIN-OBJECTS-model.txt'

        rospy.loginfo('Training object classifier')
        svm_o = SVMLight()
        svm_o.train_file = example_file_o
        svm_o.model_file = model_file_o
        svm_o.kernel_type = 0 # Use linear svm
        #svm_o.kernel_type = 4 # Use multichannel chi-square kernel
        svm_o.u_value = 'mc-chi'
        svm_o.multiclass_learn()

        response.model_file_names.append(model_file_o)

        if request.use_ca_chain:
            # Train majority vote classifiers here for each affordance,
            # conditoned on each object class
            self.ca_chain_classifiers = []

            for obj in write_response.object_labels:
                response.object_labels.append(obj)

            # Setup training object class labels
            x = svm_util.read_example_file_labels(example_file_o)
            x_labels = [str(r) for r in range(1, len(response.object_labels)+1)]
            y_labels = ['-1','+1']

            for aff in write_response.affordance_labels:

                # Setup affordance class labels for training
                y_a = []
                for obj in write_response.object_labels:
                    response.object_labels.append(obj)
                    example_file_a = self.train_example_path + obj + '-' + aff + \
                        '.txt'
                    y_o = svm_util.read_example_file_labels(example_file_a)
                    y_a.extend(y_o)

                chain_classifier = ConditionalMajorityVoteClassifier()
                rospy.loginfo('Training for affordance ' + aff)
                chain_classifier.learn(x_labels, y_labels, x, y_a)
                self.ca_chain_classifiers.append(chain_classifier)

            for aff in write_response.affordance_labels:
                response.affordance_labels.append(aff)

            return response

        # Create affordance classifiers conditioned on the object class
        for obj in write_response.object_labels:
            response.object_labels.append(obj)
            rospy.loginfo('Training conditioned on object class: ' + obj)
            for aff in write_response.affordance_labels:
                example_file_a = self.train_example_path + obj + '-' + aff + \
                    '.txt'
                model_file_a = self.train_example_path + obj + '-' + aff + \
                    '-model.txt'
                rospy.loginfo('Training for affordance ' + aff)
                svm_a = SVMLight()
                svm_a.train_file = example_file_a
                svm_a.model_file = model_file_a
                svm_a.kernel_type = 4 # Use multi-channel chi-square kernel
                svm_a.u_value = 'mc-chi'
                svm_a.learn()
                response.model_file_names.append(model_file_a)

        for aff in write_response.affordance_labels:
            response.affordance_labels.append(aff)

        return response

    def handle_aff_testing(self, request):
        '''
        Callback method for the batch testing service.
        Classifies for each affordance, given the example file.
        '''
        self.test_image_path = rospy.get_param('test_image_path')
        self.test_example_path = rospy.get_param('test_example_path')
        self.train_example_path = rospy.get_param('train_example_path')
        self.test_affordance_file = rospy.get_param('test_affordance_file')
        self.test_attribute_file = rospy.get_param('test_attribute_file')

        # TODO: Allow this to call for feature extraction on multiple images too.

        base_path = None
        if request.use_current_image:
            base_path = '/tmp/'
            extract_request = ExtractFeaturesRequest()
            extract_request.example_path = base_path
            extract_request.affordance_labels = request.affordance_labels
            extract_request.attribute_labels = request.attribute_labels
            rospy.loginfo('Calling feature extractor')
            extract_response = None
            if request.use_dp:
                extract_response = self.dp_feature_extractor(extract_request)
            elif request.use_ca_full or request.use_ca_chain:
                extract_response = self.ca_feature_extractor(extract_request)
            else:
                extract_response = self.att_feature_extractor(extract_request)
        else:
            base_path = self.test_example_path

        if request.use_ca_full or request.use_ca_chain:
            return self.handle_ca_aff_testing(request, base_path)

        response = AffordanceClassifyResponse()
        attribute_prediction_lists = []

        # Perform attribute prediction
        for a_i, a in enumerate(request.attribute_labels):
            if not request.use_att:
                break
            example_file_a = base_path + a + '.txt'
            if request.use_current_image:
                example_file_a = base_path + a + '-test.txt'
            model_file_a = self.train_example_path + a + '-model.txt'
            output_file_a = self.test_example_path + a + '-output.txt'

            # Run SVM
            svm_a = SVMLight()
            svm_a.testing_file = example_file_a
            svm_a.model_file = model_file_a
            svm_a.output_file = output_file_a
            # Run Classification
            rospy.loginfo('Running classifier for attribute: ' + a)
            svm_a.classify()

            # Read in prediction responses
            rospy.loginfo('Reading prediction list for attribute: ' + a)
            attribute_prediction_lists.append(svm_a.read_att_predictions(
                    request.attribute_types[a_i]))


        # Iterate through the different images
        attribute_feature_list = []
        if not request.use_dp:
            for i in xrange(len(attribute_prediction_lists[0])):
                # Iterate through the different attributes
                for j in xrange(len(attribute_prediction_lists)):
                    # Response list is all attribute values for image 0,
                    # followed by all for image 1, ..., image N
                    response.attribute_values.append(
                        attribute_prediction_lists[j][i])

            # Create vector from the predicted attribute labels
            num_attributes = len(request.attribute_labels)
            for i in xrange(len(attribute_prediction_lists[0])):
                attribute_feature_str = ''
                for j in xrange(num_attributes):
                    if request.attribute_types[j] == 'f':
                        attribute_feature_str += (
                            str(j+1) + ':' +
                            str(response.attribute_values[i*num_attributes + j])
                            + ' ')
                    else:
                        attribute_feature_str += ( str(j+1) + ':' +
                                                   str(sign(int(
                                        response.attribute_values[
                                            i*num_attributes + j])))
                                                   + ' ')
                attribute_feature_str.rstrip()
                attribute_feature_list.append(attribute_feature_str)

            # Write attribute feature based example file to disk for use in
            # prediction
            example_files = []
            for a in request.affordance_labels:
                example_file_a = base_path + a + '-att-feat.txt'
                if request.use_current_image:
                    example_file_a = base_path + a + '-att-feat-online.txt'
                example_file = file(example_file_a,'w')
                example_files.append(example_file)

            # Setup the attribute based example files
            affordance_label_file = file(self.test_affordance_file, 'r')
            throwout_labels = affordance_label_file.readline()
            for feat in attribute_feature_list:
                feat_labels = [int(d) for d in
                               affordance_label_file.readline().strip().split()]
                for i, a in enumerate(request.affordance_labels):
                    label = ''
                    if feat_labels[i]:
                        label = '+1'
                    else:
                        label = '-1'
                    example_str = label + ' ' + feat + '\n'
                    example_files[i].write(example_str)

            # Cleanup
            affordance_label_file.close()
            for f in example_files:
                f.close()

        # Perform affordance prediction
        aff_prediction_lists = []

        for a in request.affordance_labels:
            example_file_a = base_path + a + '-att-feat.txt'
            if request.use_current_image:
                example_file_a = base_path + a + '-att-feat-online.txt'
            model_file_a = self.train_example_path + a + '-att-feat-model.txt'
            output_file_a = self.test_example_path + a + '-att-feat-output.txt'

            if request.use_dp:
                example_file_a = base_path + a + '.txt'
                if request.use_current_image:
                    example_file_a = base_path + a + '-test.txt'
                model_file_a = self.train_example_path + a + '-model.txt'
                output_file_a = self.test_example_path + a + '-output.txt'

            # Run SVM
            svm_a = SVMLight()
            svm_a.testing_file = example_file_a
            svm_a.model_file = model_file_a
            svm_a.output_file = output_file_a

            # Run Classification
            if request.use_dp:
                rospy.loginfo('Running DP SVM for affordance: ' + a)
            else:
                rospy.loginfo('Running attribute based SVM for affordance: ' + a)
            svm_a.classify()

            # Read in prediction responses
            rospy.loginfo('Reading prediction list for affordance: ' + a)
            aff_prediction_lists.append(svm_a.read_predictions())

        for i in xrange(len(aff_prediction_lists[0])):
            for j in xrange(len(aff_prediction_lists)):
                response.affordance_values.append(aff_prediction_lists[j][i])

        return response

    def handle_ca_aff_testing(self, request, base_path):
        '''
        Method to perform affordance classification using classifiers
        conditioned on object class
        '''
        response = AffordanceClassifyResponse()
        use_gt_labels = False

        #
        # Predict the object class
        #
        if request.use_ca_full:
            example_file_o = base_path + 'CA-FULL-OBJECTS.txt'
            model_file_o = self.train_example_path + 'CA-FULL-OBJECTS-model.txt'
            output_file_o = self.test_example_path + 'CA-FULL-OBJECTS-output.txt'
        else:
            example_file_o = base_path + 'CA-CHAIN-OBJECTS.txt'
            model_file_o = self.train_example_path + 'CA-CHAIN-OBJECTS-model.txt'
            output_file_o = self.test_example_path + 'CA-CHAIN-OBJECTS-output.txt'

        # TODO: This is always giving a degenerate answer
        # TODO: try randomizing input, look at parameters in more detail
        svm_o = SVMLight()
        svm_o.testing_file = example_file_o
        svm_o.model_file = model_file_o
        svm_o.output_file = output_file_o
        rospy.loginfo('Running multiclass object SVM')
        svm_o.multiclass_classify()
        rospy.loginfo('Reading multiclass SVM output')

        obj_predictions = []
        if use_gt_labels:
            gt_in = file(example_file_o, 'r')
            obj_predictions = [int(d.strip().split()[0]) for d in
                               gt_in.readlines()]
        else:
            obj_predictions = svm_o.read_multiclass_predictions()

        for pred in obj_predictions:
            response.object_values.append(pred)

        if request.use_ca_chain:
            rospy.loginfo('Calling CHAIN Classifier')
            xs = [str(pred) for pred in obj_predictions]
            for i, a in enumerate(request.affordance_labels):
                rospy.loginfo('Classifying for affordance: ' + a)
                output_file_a = self.test_example_path + a + \
                    '-CA-CHAIN-output.txt'
                self.ca_chain_classifiers[i].batch_classify(xs, output_file_a)
            return response

        #
        # Setup conditional example files based on object class
        #

        example_files = []
        for obj in request.object_labels:
            obj_example_files = []
            for aff in request.affordance_labels:
                example_file_name_a = base_path + obj + '-' + aff + '.txt'
                example_file_a = file(example_file_name_a, 'w')
                obj_example_files.append(example_file_a)
            example_files.append(obj_example_files)

        # Read ground truth labels and features
        aff_labels = file(self.test_affordance_file,'r')
        aff_feats = file(example_file_o, 'r')

        # Skip the header
        throwout_labels = aff_labels.readline()

        for o in obj_predictions:
            feat = aff_feats.readline()[1:]
            feat_labels = [int(d) for d in aff_labels.readline().strip().split()]

            for i, aff in enumerate(request.affordance_labels):
                label = ''
                if feat_labels[i]:
                    label = '+1'
                else:
                    label = '-1'

                example_str = label + feat
                example_files[o-1][i].write(example_str)

        for o, obj in enumerate(request.object_labels):
            for i, aff in enumerate(request.affordance_labels):
                example_files[o][i].close()
        #
        # Affordance prediction
        #
        aff_prediction_lists = [] # To store data from the affordance prediction

        for obj in request.object_labels:

            for aff in request.affordance_labels:
                # perform classification using those classifiers conditioned on
                # the predicted object label

                example_file_a = base_path + obj + '-' + aff + '.txt'

                if request.use_current_image:
                    pass # TODO: set this up

                model_file_a = self.train_example_path + obj + '-' + \
                    aff + '-model.txt'
                output_file_a = self.test_example_path + obj + '-' + \
                    aff + '-ca-full-output.txt'

                # Run SVM
                svm_a = SVMLight()
                svm_a.testing_file = example_file_a
                svm_a.model_file = model_file_a
                svm_a.output_file = output_file_a

                # Run Classification
                rospy.loginfo('Running CA SVM for class ' + obj +
                              ' on affordance: ' + aff)
                svm_a.classify()

                # Read in prediction responses
                rospy.loginfo('Reading prediction list for affordance: ' + aff)

                # TODO: This probably isn't the right order because of the
                # object conditional
                # NOTE: It only matters for online use and then its one at a
                # time, so it shouldn't be a problem
                aff_prediction_lists.append(svm_a.read_predictions())

        # TODO: This is probably fucked.
        for i in xrange(len(aff_prediction_lists)):
            for j in xrange(len(aff_prediction_lists[i])):
                response.affordance_values.append(aff_prediction_lists[i][j])
        return response

if __name__ == '__main__':
    node = SVMAffordanceNode()
    node.run()
