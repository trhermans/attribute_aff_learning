#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import sys

_RES_HEIGHT = 2.5
_MAX_HEIGHT = 35.0
_RES_DIAMETER = 10.0
_MAX_DIAMETER = 130.0
_MAX_WEIGHT = 2.1
_RES_WEIGHT = 0.1
_MAX_FACES = 8
_SHIFT_THRESH = 0.2
_USE_SHIFT_INVARIANCE = True

_DEBUG_LENGTH = False
_DEBUG_PARSING = False

_USE_NEW_VERSION = True

def main(args):
    input_file = file(args[0], 'r')
    output_file = file(args[1],'w')
    input_header = input_file.readline()
    output_header = '#'

    full = []

    for line_num, line in enumerate(input_file.readlines()):
        if _DEBUG_PARSING:
            print '\nParsing line: ' + str(line_num)

        sub_length = 0

        l = line.split()
        raw_sizes = l[0:2]
        raw_shape = l[2:5]
        raw_material = l[5:12]
        raw_colors = l[12:21]
        raw_weight = l[21]
        # NOTE: there may be comments followng this last parsed element

        attribute_line = []
        if _DEBUG_PARSING:
            print 'parsing sizes'

        if _USE_NEW_VERSION:
            attribute_line.extend(parse_sizes_regression(raw_sizes))
            if line_num < 1:
                for a in xrange(len(attribute_line)-sub_length):
                    output_header += ' size' + str(a)
                sub_length = len(attribute_line)
        else:
            attribute_line.extend(parse_sizes(raw_sizes))
            if line_num < 1:
                for a in xrange(len(attribute_line)-sub_length):
                    output_header += ' size' + str(a)
                sub_length = len(attribute_line)

        if _DEBUG_LENGTH:
            print 'length is', len(attribute_line)
        if _DEBUG_PARSING:
            print 'parsing shape'

        if _USE_NEW_VERSION:
            attribute_line.extend(parse_shape_boxes(raw_shape))
            if line_num < 1:
                for a in xrange(len(attribute_line)-sub_length):
                    output_header += ' shape' + str(a)
                sub_length = len(attribute_line)
        else:
            attribute_line.extend(parse_shape(raw_shape))
            if line_num < 1:
                for a in xrange(len(attribute_line)-sub_length):
                    output_header += ' shape' + str(a)
                sub_length = len(attribute_line)

        if _DEBUG_LENGTH:
            print 'length is', len(attribute_line)
        if _DEBUG_PARSING:
            print 'parsing material'

        attribute_line.extend(parse_material(raw_material))
        if line_num < 1:
            for a in xrange(len(attribute_line)-sub_length):
                output_header += ' material' + str(a)
            sub_length = len(attribute_line)

        if _DEBUG_LENGTH:
            print 'length is', len(attribute_line)
        if _DEBUG_PARSING:
            print 'parsing color'

        attribute_line.extend(parse_colors(raw_colors))
        if line_num < 1:
            for a in xrange(len(attribute_line)-sub_length):
                output_header += ' color' + str(a)
            sub_length = len(attribute_line)

        if _DEBUG_LENGTH:
            print 'length is', len(attribute_line)
        if _DEBUG_PARSING:
            print 'parsing weight'

        if _USE_NEW_VERSION:
            attribute_line.extend(parse_weight_regression(raw_weight))
            if line_num < 1:
                for a in xrange(len(attribute_line)-sub_length):
                    output_header += ' weight' + str(a)
                sub_length = len(attribute_line)
        else:
            attribute_line.extend(parse_weight(raw_weight))
            if line_num < 1:
                for a in xrange(len(attribute_line)-sub_length):
                    output_header += ' weight' + str(a)
                sub_length = len(attribute_line)

        if _DEBUG_LENGTH:
            print 'length is', len(attribute_line)

        feat_str = ''
        for att in attribute_line:
            feat_str += str(att) + ' '
        feat_str = feat_str.rstrip()
        feat_str += '\n'

        if line_num < 1:
            output_header += '\n'
            output_file.write(output_header)

        output_file.write(feat_str)

        for y, att in enumerate(attribute_line):
            if y >= len(full):
                if att == 1 or att == 0:
                    full.append(att)
                else:
                    full.append(1)
            else:
                full[y] += (att != 0)

    input_file.close()
    output_file.close()

    print 'Attributes: ' + str(full)
    print 'Num att values:', len(full)
    # TODO: Parse vector into different parts, by saving the lengths when
    # debugging above.

def parse_sizes(raw_sizes):
    height = float(raw_sizes[0])
    diameter = float(raw_sizes[1])
    sizes = []
    sizes.extend(quantize(height, _MAX_HEIGHT, _RES_HEIGHT))
    sizes.extend(quantize(diameter, _MAX_DIAMETER, _RES_DIAMETER))
    return sizes

def parse_shape(raw_shapes):
    raw_faces = int(raw_shapes[0])
    shape_att = []
    faces = []
    for i in xrange(_MAX_FACES):
        faces.append(0)
    faces[raw_faces-1] = 1
    shape_att.extend(faces)
    shapes = []
    for s in raw_shapes[1:3]:
        shapes.append(int(s))

    shape_att.extend(shapes)
    return shape_att

def parse_material(raw_material):
    material = []
    for r in raw_material:
        material.append(int(r))
    return material

def parse_colors(raw_colors):
    colors = []
    for c in raw_colors:
        colors.append(int(c))
    return colors

def parse_weight(raw_weight):
    weights = []
    weights = quantize(float(raw_weight), _MAX_WEIGHT, _RES_WEIGHT)
    return weights

def parse_sizes_regression(raw_sizes):
    height = float(raw_sizes[0])
    diameter = float(raw_sizes[1])
    return [height, diameter]

def parse_shape_boxes(raw_shapes):
    raw_faces = int(raw_shapes[0])

    shape_att = [0,0]
    # 2 Faces => 2D-boxy
    # 6 Faces => 3D-boxy
    if raw_faces == 2:
        shape_att[0] = 1
    elif raw_faces == 6:
        shape_att[1] = 1

    # Add cylindrical and spherical attributes
    for s in raw_shapes[1:3]:
        shape_att.append(int(s))

    return shape_att

def parse_weight_regression(raw_weight):
    return [float(raw_weight)]


def quantize(val, max_val, resolution):
    hist = []
    num_bins = int(max_val / resolution)
    for i in xrange(num_bins):
        hist.append(0)

    bin_loc = int(val/resolution)
    hist[bin_loc] = 1

    # Add shift invariance for border cases
    if _USE_SHIFT_INVARIANCE:
        # We want to know that the value is not in the first bin
        if bin_loc > 0:
            # Also, the value should be
            if (val - bin_loc*resolution) < _SHIFT_THRESH*resolution:
                hist[bin_loc - 1] = 1
        if bin_loc < num_bins-1:
            if ((bin_loc+1)*resolution - val) < _SHIFT_THRESH*resolution:
                hist[bin_loc + 1] = 1

    return hist

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Two few arguments!"
    else:
        main(sys.argv[1:])
