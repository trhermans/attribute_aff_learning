#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import roslib; roslib.load_manifest('attribute_aff_learning')
import rospy
import random
import time
from attribute_aff_learning.srv import *
from svm_util import *

def aff_train(write_examples, use_dp):
    srv_call = AffordanceTrainRequest()
    srv_call.write_example_files = write_examples
    srv_call.train_model_files = True

    res = None
    if use_dp:
        srv_call.use_dp = True
        srv_call.use_ca_chain = False
        srv_call.use_ca_full = False
        srv_call.use_att = False
        dp_svm = rospy.ServiceProxy('dp_aff_train', AffordanceTrain)
        res = dp_svm(srv_call)
    else:
        srv_call.use_dp = False
        srv_call.use_ca_chain = False
        srv_call.use_ca_full = False
        srv_call.use_att = True
        att_svm = rospy.ServiceProxy('attribute_aff_train', AffordanceTrain)
        res = att_svm(srv_call)

    return res

def aff_test(affordance_labels, attribute_labels, att_types, object_labels,
             use_active, use_dp):

    srv_call = AffordanceClassifyRequest()
    srv_call.affordance_labels = affordance_labels
    srv_call.attribute_labels = attribute_labels
    srv_call.attribute_types = att_types
    srv_call.object_labels = object_labels
    srv_call.use_current_image = use_active

    res = None
    if use_dp:
        srv_call.use_dp = True
        srv_call.use_ca_chain = False
        srv_call.use_ca_full = False
        srv_call.use_att = False
        dp_svm = rospy.ServiceProxy('dp_aff_test', AffordanceClassify)
        res = dp_svm(srv_call)
    else:
        srv_call.use_dp = False
        srv_call.use_ca_chain = False
        srv_call.use_ca_full = False
        srv_call.use_att = True
        att_svm = rospy.ServiceProxy('attribute_aff_test', AffordanceClassify)
        res = att_svm(srv_call)

    return res

def extract_att_features(image_path, example_path, aff_name, att_name):
    # Setup the service
    example_file_writer = rospy.ServiceProxy("attribute_example_writer",
                                             SetupExampleFiles)

    # Setup the request to perform the feature extraction
    write_request = SetupExampleFilesRequest()
    write_request.image_path = image_path
    write_request.example_path = example_path
    write_request.affordance_file = aff_name
    write_request.attribute_file = att_name
    write_request.write_example_files = True

    # Perform the feature extraction
    example_file_writer(write_request)

def extract_dp_features(image_path, example_path, aff_name):
    # Setup the service
    example_file_writer = rospy.ServiceProxy("dp_example_writer",
                                             SetupExampleFiles)

    # Setup the request to perform the feature extraction
    write_request = SetupExampleFilesRequest()
    write_request.image_path = image_path
    write_request.example_path = example_path
    write_request.affordance_file = aff_name
    write_request.write_example_files = True

    # Perform the feature extraction
    example_file_writer(write_request)

if __name__ == '__main__':
    rospy.init_node('offline_affordance_svm_node')

    #
    # Set learning parameters
    #
    use_dp = False
    use_att = not use_dp
    use_active = False
    extract_base_files = True
    # NOTE: keep this false, we extract all at start, setting it to true writes
    # the files within the service call.
    write_train_files = False


    #
    # Get parameters of file locations
    #
    train_example_path = rospy.get_param('train_example_path')
    train_image_path = rospy.get_param('train_image_path')
    train_affordance_file = rospy.get_param('train_affordance_file')
    train_attribute_file = rospy.get_param('train_attribute_file')

    test_example_path = rospy.get_param('test_example_path')
    test_image_path = rospy.get_param('test_image_path')
    test_affordance_file = rospy.get_param('test_affordance_file')
    test_attribute_file = rospy.get_param('test_attribute_file')

    #
    # Extract training featurs and write training example files
    #
    if extract_base_files:
        rospy.loginfo('Setting up leave-one-out example files')
        if use_dp:
            extract_dp_features(train_image_path, train_example_path,
                                train_affordance_file)
            extract_dp_features(test_image_path, test_example_path,
                                test_affordance_file)
        else:
            extract_att_features(train_image_path, train_example_path,
                                 train_affordance_file, train_attribute_file)
            extract_att_features(test_image_path, test_example_path,
                                 test_affordance_file, test_attribute_file)


    #
    # Read the base training data
    #
    aff_data = read_affordance_file(train_affordance_file)
    num_train_images = int(aff_data[0])
    aff_labels = aff_data[1]
    base_aff_train_values = aff_data[2]

    att_data = read_new_attribute_file(train_attribute_file)
    att_labels = att_data[0]
    att_types = att_data[1]
    base_att_train_values = att_data[2]

    #
    # Read the base testing data
    #
    aff_data = read_affordance_file(test_affordance_file)
    num_test_images = int(aff_data[0])
    aff_test_data = aff_data[2]

    att_data = read_new_attribute_file(test_attribute_file)
    att_test_data = att_data[2]

    available_indices = range(0,num_train_images)

    min_training_size = rospy.get_param('min_train_size')
    max_training_size = num_train_images
    train_step = rospy.get_param('train_increment')
    seed_val = time.time()
    random.seed(seed_val)
    rospy.loginfo('Seeding with value of: ' + str(seed_val))
    num_splits = rospy.get_param('num_data_partitions')

    #
    # Read extracted features into python for sorting and saving purposes
    #
    base_aff_train_feature_data = []
    base_att_train_feature_data = []
    for a in aff_labels:
        if use_dp:
            aff_file = train_example_path + a + '.txt'
            base_aff_train_feature_data.append(read_example_file(aff_file))
        elif use_att:
            aff_file = train_example_path + a + '-att-feat.txt'
            base_aff_train_feature_data.append(read_example_file(aff_file))
    if use_att:
        for a in att_labels:
            att_file = train_example_path + a + '.txt'
            base_att_train_feature_data.append(read_example_file(att_file))

    #
    # Read extracted testing data features
    #
    att_test_feature_data = []
    aff_test_feature_data = []
    for a in aff_labels:
        if use_dp:
            aff_file = test_example_path + a + '.txt'
            aff_test_feature_data.append(read_example_file(aff_file))
        elif use_att:
            aff_file = test_example_path + a + '-att-feat.txt'
            aff_test_feature_data.append(read_example_file(aff_file))
    if use_att:
        for a in att_labels:
            att_file = test_example_path + a + '.txt'
            att_test_feature_data.append(read_example_file(att_file))

    #
    # Iterate through different training sizes
    #
    for train_size in range(min_training_size, max_training_size, train_step):
        rospy.loginfo(' ')
        rospy.loginfo('Training size:' + str(train_size))

        # Run multiple random tests at each training size
        for s in xrange(num_splits):

            att_train_data = []
            aff_train_data = []
            att_train_feature_data = []
            aff_train_feature_data = []

            for a in aff_labels:
                aff_train_feature_data.append([])
            if use_att:
                for a in att_labels:
                    att_train_feature_data.append([])

            train_avail_indices = available_indices[:]
            train_example_name = train_example_path + str(s) + '-' + \
                str(train_size) + '-'
            test_example_name = test_example_path + str(s) + '-' + \
                str(train_size) + '-'

            for j in xrange(train_size):
                idx = train_avail_indices[random.randint(
                        0,len(train_avail_indices)-1)]
                try:
                    train_avail_indices.remove(idx)
                except ValueError:
                    print 'Issue removing value of: ' + str(idx) + ' from list'
                aff_train_data.append(base_aff_train_values[idx])

                if use_att:
                    att_train_data.append(base_att_train_values[idx])

                for k, a in enumerate(aff_labels):
                    aff_train_feature_data[k].append(
                        base_aff_train_feature_data[k][idx])
                if  use_att:
                    for k, a in enumerate(att_labels):
                        att_train_feature_data[k].append(
                            base_att_train_feature_data[k][idx])

            #
            # Setup filenames to reflect training size
            #
            att_train_name = train_attribute_file[:-4]  + '-train-' + \
                str(s) + '-' + str(train_size) + '.txt'
            att_test_name = train_attribute_file[:-4]  + '-test-' + \
                str(s) + '-' + str(train_size) + '.txt'

            aff_train_name = ''
            aff_test_name = ''
            if use_dp:
                aff_train_name = train_affordance_file[:-4] + '-train-' + \
                      str(s) + '-' + str(train_size) + '.txt'
                aff_test_name = test_affordance_file[:-4] + '-test-' + \
                      str(s) + '-' + str(train_size) + '.txt'
            else:
                aff_train_name = train_affordance_file[:-4] + '-train-' + \
                    str(s) + '-' + str(train_size) + '-att-feat.txt'
                aff_test_name = test_affordance_file[:-4] + '-test-' + \
                    str(s) + '-' + str(train_size) + '-att-feat.txt'

            #
            # Write the training files
            #
            write_affordance_file(aff_train_name, len(aff_train_data),
                                  aff_labels, aff_train_data)
            write_affordance_file(aff_test_name, len(aff_test_data), aff_labels,
                                  aff_test_data)
            if use_att:
                write_new_attribute_file(att_train_name, att_labels, att_types,
                                         att_train_data)
                write_new_attribute_file(att_test_name, att_labels, att_types,
                                         att_test_data)

            for j, a in enumerate(aff_labels):
                aff_file = ''
                if use_dp:
                    aff_file = train_example_name + a + '.txt'
                    write_example_file(aff_file, aff_train_feature_data[j])
                else:
                    aff_file = train_example_name + a + '-att-feat.txt'
                    write_example_file(aff_file, aff_train_feature_data[j])

            if use_att:
                for j, a in enumerate(att_labels):
                    att_file = train_example_name + a + '.txt'
                    write_example_file(att_file, att_train_feature_data[j])

            #
            # Write the test files to disk
            #
            for j, a in enumerate(aff_labels):
                aff_file = ''
                if use_dp:
                    aff_file = test_example_name + a + '.txt'
                    write_example_file(aff_file, aff_test_feature_data[j])
                elif use_att:
                    aff_file = test_example_name + a + '-att-feat.txt'
                    write_example_file(aff_file, aff_test_feature_data[j])

            if use_att:
                for j, a in enumerate(att_labels):
                    att_file = test_example_name + a + '.txt'
                    write_example_file(att_file, att_test_feature_data[j])

            #
            # Set parameter values for this training set
            #
            rospy.set_param('train_example_path', train_example_name)
            rospy.set_param('train_affordance_file', aff_train_name)
            rospy.set_param('train_attribute_file', att_train_name)

            #
            # Set parameter values for this test set
            #
            rospy.set_param('test_affordance_file', aff_test_name)
            rospy.set_param('test_attribute_file', att_test_name)
            rospy.set_param('test_example_path', test_example_name)

            #
            # Perform training
            #
            rospy.loginfo('Performing leave-one-out training on set ' + str(s))
            train_response = aff_train(write_train_files, use_dp)
            test_affordance_labels = train_response.affordance_labels
            test_attribute_labels = train_response.attribute_labels

            #
            # Perform testing
            #
            rospy.loginfo('Performing leave-one-out testing on set ' + str(s))
            test_response = aff_test(test_affordance_labels,
                                     test_attribute_labels,
                                     att_types, [], use_active, use_dp)
