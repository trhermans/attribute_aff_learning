#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import roslib; roslib.load_manifest('attribute_aff_learning')
import rospy
from majority_vote_classifier import *
from attribute_aff_learning.srv import *
import knn_util
import numpy as np
from k_nearest_neighbor import KNearestNeighbors
from loess import LOESS


_VIS_FEAT_CHANNEL_STOPS = [432, 688, 1200]

def sign(x):
    if x < 0:
        return -1
    if x > 0:
        return 1
    return 0

class KNNAffordanceNode:
    '''
    ROS Node that orchestrates the calling of all k-nn required to perform
    attribute based affordance classification.
    '''

    def run(self):
        '''
        Method initializes the node and spins the classifier listeners
        '''
        rospy.init_node('knn_affordance_node',log_level=rospy.DEBUG)

        # DP stuff
        self.dp_example_file_writer = rospy.ServiceProxy("dp_example_writer",
                                                         SetupExampleFiles)
        self.dp_feature_extractor = rospy.ServiceProxy('dp_feature_extractor',
                                                       ExtractFeatures)
        self.dp_train_service = rospy.Service('dp_aff_train', AffordanceTrain,
                                              self.handle_aff_training)
        self.dp_classify_service = rospy.Service('dp_aff_test',
                                                 AffordanceClassify,
                                                 self.handle_aff_testing)

        # CA stuff
        self.ca_example_file_writer = rospy.ServiceProxy('ca_example_writer',
                                                         SetupExampleFiles)
        self.ca_feature_extractor = rospy.ServiceProxy('ca_feature_extractor',
                                                       ExtractFeatures)
        # CA full stuff
        self.ca_full_train_service = rospy.Service('ca_full_aff_train',
                                                   AffordanceTrain,
                                                   self.handle_aff_training)
        self.ca_full_classify_service = rospy.Service('ca_full_aff_test',
                                                      AffordanceClassify,
                                                      self.handle_aff_testing)
        # CA chain stuff
        self.ca_chain_train_service = rospy.Service('ca_chain_aff_train',
                                                    AffordanceTrain,
                                                    self.handle_aff_training)
        self.ca_chain_classify_service = rospy.Service('ca_chain_aff_test',
                                                       AffordanceClassify,
                                                       self.handle_aff_testing)
        self.att_knn = None
        self.att_reg = None # Regression for real-valued attributes
        self.aff_knn = None
        self.obj_knn = None
        self.ca_chain_classifiers = []
        self.ca_full_classifiers = []

        # Attribute stuff
        self.att_example_file_writer = rospy.ServiceProxy(
            "attribute_example_writer", SetupExampleFiles)
        self.att_feature_extractor = rospy.ServiceProxy(
            'attribute_feature_extractor', ExtractFeatures)
        self.att_train_service = rospy.Service('attribute_aff_train',
                                               AffordanceTrain,
                                               self.handle_aff_training)
        self.att_classify_service = rospy.Service('attribute_aff_test',
                                                  AffordanceClassify,
                                                  self.handle_aff_testing)
        self.k = rospy.get_param('k_value')
        self.base_feat_length = rospy.get_param('base_feature_length')

        # Detach process
        rospy.spin()

    def handle_aff_training(self, request):
        '''
        Callback method for the training service.
        Extracts features from the dataset.
        Builds a classifier for each affordance.
        '''
        self.train_example_path = rospy.get_param('train_example_path')
        if request.use_ca_full:
            self.train_ca_file = rospy.get_param('train_ca_full_file')
        elif request.use_ca_chain:
            self.train_ca_file = rospy.get_param('train_ca_chain_file')

        write_request = SetupExampleFilesRequest()
        write_request.image_path = rospy.get_param('train_image_path')
        write_request.example_path = self.train_example_path
        write_request.affordance_file = rospy.get_param('train_affordance_file')
        write_request.attribute_file = rospy.get_param('train_attribute_file')
        if request.use_ca_full or request.use_ca_chain:
            write_request.object_file = self.train_ca_file
        write_request.write_example_files = request.write_example_files
        rospy.logdebug('Calling write service')

        write_response = None
        if request.use_dp:
            write_response = self.dp_example_file_writer(write_request)
        elif request.use_ca_full or request.use_ca_chain:
            write_response = self.ca_example_file_writer(write_request)
        else:
            write_response = self.att_example_file_writer(write_request)

        response = AffordanceTrainResponse()

        # Separate method for ca to make this one clean
        if request.use_ca_full or request.use_ca_chain:
            response = self.handle_ca_aff_training(write_response, request)
            return response

        base_features = []

        if request.use_att:
            att_labels = []
            att_reg_values = []
            # Get labels for each attribute
            for a_i, a in enumerate(write_response.attribute_labels):
                example_file_a = self.train_example_path + a + '.txt'

                # Set to use regression if the attribute type is float
                if write_response.attribute_types[a_i] == 'f':
                    regression_values = knn_util.read_example_file_float_labels(
                        example_file_a)
                    att_reg_values.append(regression_values)

                else:
                    label_values = knn_util.read_example_file_binary_labels(
                        example_file_a)
                    att_labels.append(label_values)
                response.attribute_labels.append(a)
                response.attribute_types.append(write_response.attribute_types[a_i])

            # Extract the feature vectors for the affordance and attribute cases
            example_file_a = self.train_example_path + \
                write_response.attribute_labels[0] + '.txt'
            base_features = knn_util.read_example_file_features(example_file_a,
                                                                self.base_feat_length)

            example_aff_file = self.train_example_path + \
                write_response.affordance_labels[0] + '-att-feat.txt'
            att_features = knn_util.read_example_file_att_features(
                example_aff_file, write_response.attribute_types)


            # Set the data of the knn instance
            rospy.loginfo("Setting att_knn values")
            self.att_knn = KNearestNeighbors(self.k)
            self.att_knn.exemplars = base_features
            self.att_knn.labels = np.asarray(att_labels).T
            # self.att_knn.dist_func = self.att_knn.chi_squared_dist
            rospy.loginfo('Estimating att weights')
            self.att_knn.channel_stops = _VIS_FEAT_CHANNEL_STOPS
            self.att_knn.dist_func = self.att_knn.multi_channel_chi_squared
            self.att_knn.estimate_mixed_channel_weights()

            rospy.loginfo("Setting att_reg values")
            self.att_reg = KNearestNeighbors(self.k)
            self.att_reg.exemplars = base_features
            self.att_reg.labels = np.asarray(att_reg_values).T
            # self.att_reg.dist_func = self.att_reg.chi_squared_dist
            self.att_reg.channel_stops = _VIS_FEAT_CHANNEL_STOPS
            self.att_reg.dist_func = self.att_reg.multi_channel_chi_squared
            self.att_reg.channel_weights = self.att_knn.channel_weights
            #self.att_reg.estimate_mixed_channel_weights()

        else:
            example_file_a = self.train_example_path + \
                write_response.affordance_labels[0] + '.txt'
            base_features = knn_util.read_example_file_features(
                example_file_a, self.base_feat_length)

        # Affordance stuff
        aff_label_values = []

        # Extract labels for each affordance
        for a in write_response.affordance_labels:
            example_file_a = ''
            if request.use_dp:
                example_file_a = self.train_example_path + a + '.txt'
            else:
                example_file_a = self.train_example_path + a + '-att-feat.txt'
            label_values = knn_util.read_example_file_labels(example_file_a)
            aff_label_values.append(label_values)
            response.affordance_labels.append(a)
        if request.use_dp:
            aff_features = base_features
        elif request.use_att:
            aff_features = att_features

        self.aff_knn = KNearestNeighbors(self.k)
        self.aff_knn.exemplars = aff_features
        self.aff_knn.labels = np.asarray(aff_label_values).T
        if request.use_dp:
            rospy.loginfo('Estimating weights')
            self.aff_knn.channel_stops = _VIS_FEAT_CHANNEL_STOPS
            self.aff_knn.dist_func = self.aff_knn.multi_channel_chi_squared
            self.aff_knn.estimate_mixed_channel_weights()
        else:
            rospy.loginfo('Estimating att->aff weights')
            self.aff_knn.feat_types = np.asarray(write_response.attribute_types)
            self.aff_knn.dist_func = self.aff_knn.split_att_metric
            self.aff_knn.estimate_binary_float_channel_weights()

        return response

    def handle_ca_aff_training(self, write_response, request):
        '''
        Performs training of the object class classifiers as well as the
        affordance classifiers conditioned on those object classes.
        '''
        response = AffordanceTrainResponse()

        # Train multiclass classifier for object classes
        if request.use_ca_full:
            example_file_o = self.train_example_path + 'CA-FULL-OBJECTS.txt'
        else:
            example_file_o = self.train_example_path + 'CA-CHAIN-OBJECTS.txt'

        obj_labels = knn_util.read_example_file_labels(example_file_o)
        obj_features = knn_util.read_example_file_features(example_file_o,
                                                           self.base_feat_length)

        rospy.loginfo('Training object classifier')
        self.obj_knn = KNearestNeighbors(self.k)
        self.obj_knn.labels = obj_labels
        self.obj_knn.exemplars = obj_features
        rospy.loginfo('Estimating weights')
        self.obj_knn.channel_stops = _VIS_FEAT_CHANNEL_STOPS
        # self.obj_knn.dist_func = self.obj_knn.chi_squared_dist
        self.obj_knn.dist_func = self.obj_knn.multi_channel_chi_squared
        self.obj_knn.estimate_mixed_channel_weights()

        if request.use_ca_chain:
            # Train majority vote classifiers here for each affordance,
            # conditoned on each object class
            self.ca_chain_classifiers = []

            for obj in write_response.object_labels:
                response.object_labels.append(obj)

            # Setup training object class labels
            x = knn_util.read_example_file_labels(example_file_o)
            x_labels = [str(r) for r in range(1, len(response.object_labels)+1)]
            y_labels = ['-1','+1']

            for aff in write_response.affordance_labels:

                # Setup affordance class labels for training
                y_a = []
                for obj in write_response.object_labels:
                    response.object_labels.append(obj)
                    example_file_a = self.train_example_path + obj + '-' + aff + \
                        '.txt'
                    y_o = knn_util.read_example_file_labels(example_file_a)
                    y_a.extend(y_o)

                chain_classifier = ConditionalMajorityVoteClassifier()
                rospy.loginfo('Training for affordance ' + aff)
                chain_classifier.learn(x_labels, y_labels, x, y_a)
                self.ca_chain_classifiers.append(chain_classifier)

            for aff in write_response.affordance_labels:
                response.affordance_labels.append(aff)

            return response

        #
        # CA-Full
        #

        # Create affordance classifiers conditioned on the object class
        for obj in write_response.object_labels:
            response.object_labels.append(obj)
            rospy.loginfo('Training conditioned on object class: ' + obj)

            # Read the affordance labels for each affordance for object class
            aff_labels = []
            for aff in write_response.affordance_labels:
                example_file_a = self.train_example_path + obj + '-' + aff + \
                    '.txt'
                label_values = knn_util.read_example_file_labels(example_file_a)
                aff_labels.append(label_values)

            # Read the object feature data for each object class
            aff_features = knn_util.read_example_file_features(example_file_a,
                                                               self.base_feat_length)
            aff_o_knn = KNearestNeighbors(self.k)
            aff_o_knn.exemplars = aff_features
            aff_o_knn.labels = np.asarray(aff_labels).T
            # aff_o_knn.dist_func = aff_o_knn.chi_squared_dist
            rospy.loginfo('Estimating weights')
            aff_o_knn.channel_stops = _VIS_FEAT_CHANNEL_STOPS
            aff_o_knn.dist_func = aff_o_knn.multi_channel_chi_squared
            aff_o_knn.estimate_mixed_channel_weights()

            self.ca_full_classifiers.append(aff_o_knn)

        for aff in write_response.affordance_labels:
            response.affordance_labels.append(aff)

        return response

    def handle_aff_testing(self, request):
        '''
        Callback method for the batch testing service.
        Classifies for each affordance, given the example file.
        '''
        self.test_example_path = rospy.get_param('test_example_path')
        self.test_affordance_file = rospy.get_param('test_affordance_file')

        base_path = self.test_example_path

        if request.use_ca_full or request.use_ca_chain:
            return self.handle_ca_aff_testing(request, base_path)

        response = AffordanceClassifyResponse()
        attribute_prediction_lists = []

        # Perform attribute prediction
        if request.use_att:
            example_file_a = base_path + request.attribute_labels[0] + '.txt'
            base_features = knn_util.read_example_file_features(
                example_file_a, self.base_feat_length)

            # Get predicted attribute values
            rospy.loginfo('Running attribute regression')
            pred_reg_values = self.att_reg.regress(base_features)
            rospy.loginfo('Running attribute classifier ')
            pred_bin_values = self.att_knn.classify(base_features)

            # Write attribute values to disk and create attribute featue vectors
            bin_idx = 0
            reg_idx = 0
            att_features = []
            for a, t in zip(request.attribute_labels, request.attribute_types):
                output_file_a = self.test_example_path + a + '-output.txt'
                if t == 'f':
                    knn_util.write_output_file(output_file_a,
                                               pred_reg_values[:,reg_idx])
                    att_features.append(pred_reg_values[:,reg_idx])
                    reg_idx += 1
                else:
                    knn_util.write_output_file(output_file_a,
                                               pred_bin_values[:,bin_idx])
                    att_features.append(pred_bin_values[:,bin_idx])
                    bin_idx += 1
            # Convert att features to the correct format
            att_features = np.asarray(att_features).T

        #
        # Perform affordance prediction
        #
        aff_prediction_lists = []

        a = request.affordance_labels[0]
        if request.use_dp:
            example_file_a = base_path + a + '.txt'
            base_features = knn_util.read_example_file_features(
                example_file_a, self.base_feat_length)

        # Run Classification
        if request.use_dp:
            rospy.loginfo('Running DP KNN for affordances')
            aff_features = base_features
        else:
            rospy.loginfo('Running attribute based KNN for affordances')
            aff_features = att_features

        aff_pred_values = self.aff_knn.classify(aff_features)

        # Write prediction responses
        for i, a in enumerate(request.affordance_labels):
            rospy.loginfo('Writing prediction list for affordance: ' + a)
            if request.use_dp:
                output_file_a = self.test_example_path + a + '-output.txt'
            else:
                output_file_a = self.test_example_path + a + '-att-feat-output.txt'
            knn_util.write_output_file(output_file_a, aff_pred_values[:,i])

        # TODO: Append predicted values to response
        response.affordance_values = []

        return response

    def handle_ca_aff_testing(self, request, base_path):
        '''
        Method to perform affordance classification using classifiers
        conditioned on object class
        '''
        response = AffordanceClassifyResponse()
        use_gt_labels = True

        #
        # Predict the object class
        #
        if request.use_ca_full:
            example_file_o = base_path + 'CA-FULL-OBJECTS.txt'
            output_file_o = self.test_example_path + 'CA-FULL-OBJECTS-output.txt'
        else:
            example_file_o = base_path + 'CA-CHAIN-OBJECTS.txt'
            output_file_o = self.test_example_path + 'CA-CHAIN-OBJECTS-output.txt'

        query_features = knn_util.read_example_file_features(example_file_o,
                                                             self.base_feat_length)

        rospy.loginfo('Running multiclass object k-nn')
        obj_predictions = self.obj_knn.classify(query_features)

        # Write object results to disk
        knn_util.write_output_file(output_file_o, obj_predictions)

        #
        # CA-Chain
        #
        if request.use_ca_chain:
            rospy.loginfo('Calling CHAIN Classifier')
            xs = [str(pred) for pred in obj_predictions]
            for i, a in enumerate(request.affordance_labels):
                rospy.loginfo('Classifying for affordance: ' + a)
                output_file_a = self.test_example_path + a + \
                    '-CA-CHAIN-output.txt'
                self.ca_chain_classifiers[i].batch_classify(xs, output_file_a)
            return response

        #
        # CA-Full
        #

        #
        # Affordance prediction
        #
        conditional_features = []
        for obj in request.object_labels:
            conditional_features.append([])

        # Iterate through the predicted object class labels adding feature
        # vectors to the o-1 feature list
        for pred, feat in zip(obj_predictions, query_features):
            conditional_features[int(pred)-1].append(feat)

        # Run knn for each object class
        for o, obj in enumerate(request.object_labels):
            rospy.loginfo('Running affordance classifier for class ' + obj)
            aff_responses = self.ca_full_classifiers[o].classify(
                np.asarray(conditional_features[o]))

            # Write the output files
            for i, aff in enumerate(request.affordance_labels):
                output_file_a = self.test_example_path + obj + '-' + \
                    aff + '-ca-full-output.txt'
                knn_util.write_output_file(output_file_a, aff_responses[:,i])

            # TODO: Write output to response message
            # knn_util.write_output_file()
        return response

if __name__ == '__main__':
    node = KNNAffordanceNode()
    node.run()
