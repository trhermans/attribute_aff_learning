#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.


import roslib; roslib.load_manifest('attribute_aff_learning')
import rospy
import numpy as np

def read_affordance_file(filename):
    data_in = file(filename, 'r')
    header_info = data_in.readline().split()[1:]
    num_images = header_info[0]
    affordance_labels = header_info[1:]
    data = [d.rstrip().split() for d in data_in.readlines()]
    data_in.close()
    return (num_images, affordance_labels, data)

def read_attribute_file(filename):
    data_in = file(filename, 'r')
    header_info = data_in.readline().split()[1:]
    attribute_labels = header_info[0:]
    data = [d.rstrip().split() for d in data_in.readlines()]
    data_in.close()
    return (attribute_labels, data)

def read_new_attribute_file(filename):
    data_in = file(filename, 'r')
    header_info = data_in.readline().split()[1:]
    attribute_labels = header_info[0:]
    header_info = data_in.readline().split()[1:]
    attribute_types = header_info[0:]
    data = [d.rstrip().split() for d in data_in.readlines()]
    data_in.close()
    return (attribute_labels, attribute_types, data)

def read_object_file(filename):
    data_in = file(filename, 'r')
    header_info = data_in.readline().split()[1:]
    num_images = header_info[0]
    object_labels = header_info[1:]
    data = [d.rstrip().split() for d in data_in.readlines()]
    data_in.close()
    return (object_labels, data)

def write_affordance_file(filename, num_imgs, affordance_labels, data):
    data_out = file(filename, 'w')
    data_out.write('# ' + str(num_imgs))
    for a in affordance_labels:
        data_out.write(' ' + a)
    data_out.write('\n')

    for d in data:
        data_line = ''
        for e in d:
            data_line += str(e) + ' '
        data_line.rstrip()
        data_line += '\n'
        data_out.write(data_line)
    data_out.close()

def write_attribute_file(filename, attribute_labels, data):
    data_out = file(filename, 'w')
    data_out.write('#')
    for a in attribute_labels:
        data_out.write(' ' + a)
    data_out.write('\n')
    for d in data:
        data_line = ''
        for e in d:
            data_line += str(e) + ' '
        data_line.rstrip()
        data_line += '\n'
        data_out.write(data_line)
    data_out.close()

def write_new_attribute_file(filename, attribute_labels, attribute_types, data):
    data_out = file(filename, 'w')
    data_out.write('#')
    for a in attribute_labels:
        data_out.write(' ' + a)
    data_out.write('\n')
    data_out.write('#')
    for a in attribute_types:
        data_out.write(' ' + a)
    data_out.write('\n')
    for d in data:
        data_line = ''
        for e in d:
            data_line += str(e) + ' '
        data_line.rstrip()
        data_line += '\n'
        data_out.write(data_line)
    data_out.close()

def write_object_file(filename, num_imgs, object_labels, data):
    data_out = file(filename, 'w')
    data_out.write('# ' + str(num_imgs))
    for a in object_labels:
        data_out.write(' ' + a)
    data_out.write('\n')

    for d in data:
        data_line = ''
        for e in d:
            data_line += str(e) + ' '
        data_line.rstrip()
        data_line += '\n'
        data_out.write(data_line)
    data_out.close()

def read_example_file(filename):
    data_in = file(filename, 'r')
    feature_lines = data_in.readlines()
    data_in.close()
    return feature_lines

def write_example_file(filename, data):
    data_out = file(filename, 'w')
    data_out.writelines(data)
    data_out.close()

def read_image_loc_file(filename):
    data_in = file(filename, 'r')
    image_locs = data_in.readlines()
    data_in.close()
    return image_locs

def write_image_loc_file(filename, data):
    data_out = file(filename, 'w')
    data_out.writelines(data)
    data_out.close()

def read_example_file_labels(filename):
    data_in = file(filename, 'r')
    labels = [r.split()[0] for r in data_in.readlines()]
    data_in.close()
    return np.asarray(labels)

def read_example_file_float_labels(filename):
    data_in = file(filename, 'r')
    labels = [float(r.split()[0]) for r in data_in.readlines()]
    data_in.close()
    return np.asarray(labels)

def read_example_file_binary_labels(filename):
    data_in = file(filename, 'r')
    labels = [0.5*(int(r.split()[0])+1) for r in data_in.readlines()]
    data_in.close()
    return np.asarray(labels)

def make_dense(line, length):
    dense_line = np.zeros(length)
    for feat in line:
        idx, value = feat.split(':')
        idx = int(idx)-1 # SVMLight uses 1 based indexing
        value = float(value)
        dense_line[idx] = value
    return dense_line

def make_att_dense(line, types):
    dense_line = np.zeros(len(types))
    for feat in line:
        idx, value = feat.split(':')
        idx = int(idx)-1 # SVMLight uses 1 based indexing
        value = float(value)
        if types[idx] == 'b':
            value = 0.5*(value+1)
        dense_line[idx] = value
    return dense_line

def read_example_file_features(filename, feat_length):
    data_in = file(filename, 'r')
    sparse_feature_lines = [r.split()[1:] for r in data_in.readlines()]
    data_in.close()
    dense_features = []
    for i, line in enumerate(sparse_feature_lines):
        dense_line = make_dense(line, feat_length)
        dense_features.append(dense_line)

    return np.asarray(dense_features)

def read_example_file_att_features(filename, types):
    data_in = file(filename, 'r')
    sparse_feature_lines = [r.split()[1:] for r in data_in.readlines()]
    data_in.close()
    dense_features = []
    for i, line in enumerate(sparse_feature_lines):
        dense_line = make_att_dense(line, types)
        dense_features.append(dense_line)

    return np.asarray(dense_features)

def write_output_file(filename, predictions):
    data_out = file(filename, 'w')
    data_out.writelines([str(p)+'\n' for p in predictions])
    data_out.close()
