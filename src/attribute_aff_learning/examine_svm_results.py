#!/usr/bin/env python

import matplotlib.pyplot as plotter
from math import sqrt

_IMG_BASE = '/home/thermans/data/mars_lab_aff_pics/imgs/'
_BASE_BASE = '/home/thermans/data/mars_lab_aff_pics/'
_NUM_SETS = 5
_USE_ERROR_BARS = True
_OBJECT_LABELS = ['BALL', 'BOOK', 'BOX', 'MUG', 'SHOE', 'TOWEL']
_AFF_LABELS = ['PUSHABLE', 'ROLLABLE', 'GRASPABLE', 'LIFTABLE', 'DRAGABLE',
               'CARRYABLE', 'TRAVERSABLE']

_NEW_ATT_LABELS = ['size0', 'size1',
                   'shape0', 'shape1', 'shape2', 'shape3',
                   'material0', 'material1', 'material2', 'material3',
                   'material4', 'material5', 'material6',
                   'color0', 'color1', 'color2', 'color3', 'color4', 'color5',
                   'color6', 'color7', 'color8',
                   'weight0']
_NEW_ATT_REAL_LABELS = [['height', 'diameter'],
                        ['2 Faces', '6 Faces', 'CYLINDRICAL', 'SPHERICAL'],
                        ['CLOTH', 'CERAMIC/GLASS', 'METAL', 'PAPER', 'PLASTIC',
                         'RUBBER', 'WOOD'],
                        ['YELLOW', 'BLUE', 'GREEN', 'ORGANGE',
                         'PURPLE', 'BLACK', 'WHITE', 'GRAY', 'BROWN'],
                        ['weight']]
_NEW_ATT_TYPE = ['f', 'f', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b',
                 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'f']
_ATT_LABELS = _NEW_ATT_LABELS
_ATT_REAL_LABELS = _NEW_ATT_REAL_LABELS

def sign(x):
    if x < 0:
        return -1
    if x > 0:
        return 1
    return 0

def read_multiclass_output_file(file_name):
    results = file(file_name, 'r')
    vals = [(int(x[0])) for x in [r.split() for r in \
                                            [l.strip() for l in results]]]
    return vals

def read_binary_output_file(file_name):
    results = file(file_name, 'r')
    vals = [sign(float(x[0])) for x in [r.split() for r in \
                                            [l.strip() for l in results]]]
    return vals

def compare_binary_files(gt_file_name, pred_file_name):
    gt = read_binary_output_file(gt_file_name)
    pred = read_binary_output_file(pred_file_name)

    correct = []
    true_positives = []
    false_positives = []
    true_negatives = []
    false_negatives = []

    for i in xrange(len(gt)):
        true_positives.append(0)
        false_positives.append(0)
        true_negatives.append(0)
        false_negatives.append(0)

        agree = (gt[i] == pred[i])

        correct.append(agree)

        # Set the value for true positives, etc
        if gt[i] > 0:
            if agree:
                true_positives[i] = 1
            else:
                false_negatives[i] = 1
        else:
            if agree:
                true_negatives[i] = 1
            else:
                false_positives[i] = 1

    return (true_positives, false_positives, true_negatives, false_negatives,
            correct)

def compare_multiclass_files(gt_file_name, pred_file_name):
    gt = read_multiclass_output_file(gt_file_name)
    pred = read_multiclass_output_file(pred_file_name)

    # TODO: Add confusion matrix
    correct = []
    true_positives = []
    false_positives = []
    true_negatives = []
    false_negatives = []

    for i in xrange(len(gt)):
        true_positives.append(0)
        false_positives.append(0)
        true_negatives.append(0)
        false_negatives.append(0)

        agree = (gt[i] == pred[i])

        correct.append(agree)

        # Set the value for true positives, etc
        if gt[i] > 0:
            if agree:
                true_positives[i] = 1
            else:
                false_negatives[i] = 1
        else:
            if agree:
                true_negatives[i] = 1
            else:
                false_positives[i] = 1

    return (true_positives, false_positives, true_negatives, false_negatives,
            correct)

def build_file_hist(file_name, has_header, hist_label='', xlabel='', ylabel='',
                    hist_color='blue'):
    data_in = file(file_name, 'r')

    if has_header:
        header = data_in.readline()

    data = [line.strip().split() for line in data_in.readlines()]
    num_objects = float(len(data))
    print 'Num_objects:', len(data)

    hist = []
    for i, x in enumerate(data[0]):
        if x.isdigit(): # Deal with comments
            hist.append(0)

    for row in data:
        for i, x in enumerate(row):
            if x.isdigit(): # Deal with comments
                hist[i] += int(x)

    for i, x in enumerate(hist):
        hist[i] = x / num_objects


    print 'Histogram has', len(hist), 'elements'
    plotter.bar(xrange(len(hist)), hist, label=hist_label, color=hist_color)

    plotter.title(hist_label, fontsize=18)
    plotter.xlabel(xlabel, fontsize=15)
    plotter.ylabel(ylabel, fontsize=18)
    plotter.ylim((0,1))
    plotter.draw()


#
# I/O Methods
#

def display_line(ls):
    l_line = '  '
    for l in ls:
        l_line += '  ' + str(l)
    print l_line

def display_line_sum(ls, name):
    l_line = name + '    '
    for l in ls:
        l_line += str(sum(l)) + '  \t  '
    print l_line

def display_att_line_sum(ls, name):
    l_line = name + '  '
    for l in ls:
        l_line += str(sum(l)) + '  '
    print l_line

def print_tfnp_table(labels, tps, fps, tns, fns):
    display_line(labels)
    display_line_sum(tps, 'True Pos ')
    display_line_sum(fps, 'False Pos')
    display_line_sum(tns, 'True Neg ')
    display_line_sum(fns, 'False Neg')

    total_true = 0
    total_false = 0

    correct_rates = []
    correct_line = '% correct: '

    for i, tp in enumerate(tps):
        true_sum = sum(tps[i]) + sum(tns[i])
        false_sum = sum(fps[i]) + sum(fns[i])
        total_true += true_sum
        total_false += false_sum
        correct = true_sum / float(true_sum + false_sum)
        correct_rates.append(correct)
        correct_line += '\t%g' % (correct*100.0)

    print correct_line

    total_correct = total_true / float(total_true + total_false)
    total_line = 'total:\t%g\n' % (total_correct*100.0)
    print total_line
    return (total_true, total_false)

def print_att_tfnp_table(labels, tps, fps, tns, fns):
    #display_line(labels)
    print 'Attributes'
    display_att_line_sum(tps, 'True Pos ')
    display_att_line_sum(fps, 'False Pos')
    display_att_line_sum(tns, 'True Neg ')
    display_att_line_sum(fns, 'False Pos')

    total_true = 0
    total_false = 0

    correct_rates = []
    correct_line = '% correct: '

    for i, tp in enumerate(tps):
        true_sum = sum(tps[i]) + sum(tns[i])
        false_sum = sum(fps[i]) + sum(fns[i])
        total_true += true_sum
        total_false += false_sum
        correct = true_sum / float(true_sum + false_sum)
        correct_rates.append(correct)
        correct_line += '  %g' % (correct*100.0)

    print correct_line

    total_correct = total_true / float(total_true + total_false)
    total_line = 'total:\t%g\n' % (total_correct*100.0)
    print total_line


#
# High level functions
#

def check_dp_results(base_name, train_size, name='Att'):

    print name

    num_sets = _NUM_SETS

    all_total_true = 0
    all_total_false = 0

    total_aff_tps =[]
    total_aff_fps = []
    total_aff_tns = []
    total_aff_fns = []

    for a, aff in enumerate(_AFF_LABELS):
        total_aff_tps.append([])
        total_aff_fps.append([])
        total_aff_tns.append([])
        total_aff_fns.append([])

    for i in xrange(num_sets):
        tps =[]
        fps = []
        tns = []
        fns = []
        corrects = []

        start_name = base_name+'test-'+str(i)+'-'+str(train_size)+'-'
        for a, aff in enumerate(_AFF_LABELS):
            gt_file = start_name + aff + '.txt'
            pred_file = start_name + aff + '-output.txt'

            tp, fp, tn, fn, res = compare_binary_files(gt_file, pred_file)
            tps.append(tp)
            fps.append(fp)
            tns.append(tn)
            fns.append(fn)
            corrects.append(res)

            total_aff_tps[a].extend(tp)
            total_aff_fps[a].extend(fp)
            total_aff_tns[a].extend(tn)
            total_aff_fns[a].extend(fn)

        (total_true, total_false) = print_tfnp_table(_AFF_LABELS, tps, fps, tns,
                                                     fns)

        all_total_true += total_true
        all_total_false += total_false


        full_labels = []
        for i in xrange(len(corrects[0])):
            img_affs = []
            for j, aff in enumerate(_AFF_LABELS):
                img_affs.append(corrects[j][i])
            full_labels.append(img_affs)

        total_corrects = 0
        for img in full_labels:
            still_true = True
            for aff in img:
                if not aff:
                    still_true = False
            if still_true:
                total_corrects += 1

        print 'Number of fully corrects affordance vectors:', total_corrects, '\n'

    for a, aff in enumerate(_AFF_LABELS):
        trues = sum(total_aff_tps[a]) + sum(total_aff_tns[a])
        falses = sum(total_aff_fps[a]) + sum(total_aff_fns[a])
        correct = trues / float(trues+falses)*100.0
        print 'Overal % correct for', aff, correct

    correct_percent = total_true / float(total_true + total_false)*100.0
    print 'Overall percent correct: ', correct_percent

def check_att_results(base_name, train_size, name='Att'):

    print name
    num_sets = _NUM_SETS

    all_total_true = 0
    all_total_false = 0

    total_aff_tps =[]
    total_aff_fps = []
    total_aff_tns = []
    total_aff_fns = []

    for a, aff in enumerate(_AFF_LABELS):
        total_aff_tps.append([])
        total_aff_fps.append([])
        total_aff_tns.append([])
        total_aff_fns.append([])

    for i in xrange(num_sets):
        aff_tps =[]
        aff_fps = []
        aff_tns = []
        aff_fns = []
        aff_corrects = []
        start_name = base_name+'test-'+str(i)+'-'+str(train_size)+'-'
        for a, aff in enumerate(_AFF_LABELS):

            gt_file = start_name + aff + '-att-feat.txt'
            pred_file = start_name + aff + '-att-feat-output.txt'

            tp, fp, tn, fn, res = compare_binary_files(gt_file, pred_file)
            aff_tps.append(tp)
            aff_fps.append(fp)
            aff_tns.append(tn)
            aff_fns.append(fn)
            aff_corrects.append(res)

            total_aff_tps[a].extend(tp)
            total_aff_fps[a].extend(fp)
            total_aff_tns[a].extend(tn)
            total_aff_fns[a].extend(fn)

        (total_true, total_false) = print_tfnp_table(_AFF_LABELS, aff_tps,
                                                     aff_fps, aff_tns, aff_fns)
        all_total_true += total_true
        all_total_false += total_false


        full_labels = []
        for i in xrange(len(aff_corrects[0])):
            img_affs = []
            for j, aff in enumerate(_AFF_LABELS):
                img_affs.append(aff_corrects[j][i])
            full_labels.append(img_affs)

        total_aff_corrects = 0
        for img in full_labels:
            still_true = True
            for aff in img:
                if not aff:
                    still_true = False
            if still_true:
                total_aff_corrects += 1

        print 'Number of fully corrects affordance vectors:', \
            total_aff_corrects, '\n'

        att_tps =[]
        att_fps = []
        att_tns = []
        att_fns = []
        att_corrects = []

        for att in _ATT_LABELS:
            break
            gt_file = base_name + 'test-' + str(i) + '-' + att + '.txt'
            pred_file = base_name + 'test-' + str(i) + '-' + att + '-output.txt'

            if no_splits:
                gt_file = base_name + att + '.txt'
                pred_file = base_name + att + '-output.txt'

            tp, fp, tn, fn, res = compare_binary_files(gt_file, pred_file)
            att_tps.append(tp)
            att_fps.append(fp)
            att_tns.append(tn)
            att_fns.append(fn)
            att_corrects.append(res)

        # print_att_tfnp_table(_ATT_LABELS, att_tps, att_fps, att_tns, att_fns)


    for a, aff in enumerate(_AFF_LABELS):
        trues = sum(total_aff_tps[a]) + sum(total_aff_tns[a])
        falses = sum(total_aff_fps[a]) + sum(total_aff_fns[a])
        correct = trues / float(trues+falses)*100.0
        print 'Overal % correct for', aff, correct

    correct_percent = total_true / float(total_true + total_false)*100.0
    print 'Overall percent correct: ', correct_percent

def check_ca_full_results(base_name, train_size, name='CA-FULL'):
    print name
    print_obj_info = False

    all_total_true = 0
    all_total_false = 0

    total_aff_tps =[]
    total_aff_fps = []
    total_aff_tns = []
    total_aff_fns = []

    for a, aff in enumerate(_AFF_LABELS):
        total_aff_tps.append([])
        total_aff_fps.append([])
        total_aff_tns.append([])
        total_aff_fns.append([])

    for i in xrange(_NUM_SETS):
        aff_tps =[]
        aff_fps = []
        aff_tns = []
        aff_fns = []
        aff_corrects = []

        for a, aff in enumerate(_AFF_LABELS):
            obj_tps = []
            obj_fps = []
            obj_tns = []
            obj_fns = []
            obj_res = []
            for obj in _OBJECT_LABELS:
                # TODO: This data needs to be saved better
                gt_file = base_name + 'test-' + str(i) + '-' \
                    + str(train_size) + '-' + obj + '-' + aff + '.txt'
                pred_file = base_name + 'test-' + str(i) + '-'\
                    + str(train_size) + '-' + obj + '-' + aff + '-ca-full-output.txt'
                tp, fp, tn, fn, res = compare_binary_files(gt_file, pred_file)

                obj_tps.extend(tp)
                obj_fps.extend(fp)
                obj_tns.extend(tn)
                obj_fns.extend(fn)
                obj_res.extend(res)

            aff_tps.append(obj_tps)
            aff_fps.append(obj_fps)
            aff_tns.append(obj_tns)
            aff_fns.append(obj_fns)
            aff_corrects.append(obj_res)

            total_aff_tps[a].extend(obj_tps)
            total_aff_fps[a].extend(obj_fps)
            total_aff_tns[a].extend(obj_tns)
            total_aff_fns[a].extend(obj_fns)


        # TODO: This needs to be organized better
        (total_true, total_false) = print_tfnp_table(_AFF_LABELS, aff_tps,
                                                     aff_fps, aff_tns, aff_fns)
        all_total_true += total_true
        all_total_false += total_false

        full_labels = []
        for k in xrange(len(aff_corrects[0])):
            img_affs = []
            for j, aff in enumerate(_AFF_LABELS):
                img_affs.append(aff_corrects[j][k])
            full_labels.append(img_affs)

        total_aff_corrects = 0
        for img in full_labels:
            still_true = True
            for aff in img:
                if not aff:
                    still_true = False
            if still_true:
                total_aff_corrects += 1

        print 'Number of fully corrects affordance vectors:', \
            total_aff_corrects, '\n'

        obj_tps =[]
        obj_fps = []
        obj_tns = []
        obj_fns = []
        obj_corrects = []

        obj_gt_file = base_name + 'test-' + str(i) + '-' + str(train_size) + \
            '-CA-FULL-OBJECTS.txt'
        obj_pred_file = base_name + 'test-' + str(i) +'-' + str(train_size) +\
            '-CA-FULL-OBJECTS-output.txt'
        obj_data = compare_multiclass_files(obj_gt_file, obj_pred_file)

        obj_tps.extend(obj_data[0])
        obj_fps.extend(obj_data[1])
        obj_tns.extend(obj_data[2])
        obj_fns.extend(obj_data[3])
        obj_corrects.extend(obj_data[4])

        if print_obj_info:
            print '\nObject classification:'
            print 'True Pos ', sum(obj_tps)
            print 'False Pos', sum(obj_fps)
            print 'True Neg ', sum(obj_tns)
            print 'False Neg', sum(obj_fns)
            print ''

    for a, aff in enumerate(_AFF_LABELS):
        trues = sum(total_aff_tps[a]) + sum(total_aff_tns[a])
        falses = sum(total_aff_fps[a]) + sum(total_aff_fns[a])
        correct = trues / float(trues+falses)*100.0
        print 'Overal % correct for', aff, correct

    correct_percent = total_true / float(total_true + total_false)*100.0
    print 'Overall percent correct: ', correct_percent

def check_ca_full_knn_results(base_name, train_size, name='CA-FULL'):
    print name
    print_obj_info = False

    all_total_true = 0
    all_total_false = 0

    total_aff_tps =[]
    total_aff_fps = []
    total_aff_tns = []
    total_aff_fns = []

    for a, aff in enumerate(_AFF_LABELS):
        total_aff_tps.append([])
        total_aff_fps.append([])
        total_aff_tns.append([])
        total_aff_fns.append([])

    for i in xrange(_NUM_SETS):
        aff_tps =[]
        aff_fps = []
        aff_tns = []
        aff_fns = []
        aff_corrects = []

        for a, aff in enumerate(_AFF_LABELS):
            # TODO: This data needs to be saved better
            gt_file = base_name + 'test-' + str(i) + '-' \
                + str(train_size) + '-' + aff + '-ca-full.txt'
            pred_file = base_name + 'test-' + str(i) + '-'\
                + str(train_size) +  '-' + aff + '-ca-full-output.txt'
            tp, fp, tn, fn, res = compare_binary_files(gt_file, pred_file)

            aff_tps.append(tp)
            aff_fps.append(fp)
            aff_tns.append(tn)
            aff_fns.append(fn)
            aff_corrects.append(res)

            total_aff_tps[a].extend(tp)
            total_aff_fps[a].extend(fp)
            total_aff_tns[a].extend(tn)
            total_aff_fns[a].extend(fn)


        # TODO: This needs to be organized better
        (total_true, total_false) = print_tfnp_table(_AFF_LABELS, aff_tps,
                                                     aff_fps, aff_tns, aff_fns)
        all_total_true += total_true
        all_total_false += total_false

        full_labels = []
        for k in xrange(len(aff_corrects[0])):
            img_affs = []
            for j, aff in enumerate(_AFF_LABELS):
                img_affs.append(aff_corrects[j][k])
            full_labels.append(img_affs)

        total_aff_corrects = 0
        for img in full_labels:
            still_true = True
            for aff in img:
                if not aff:
                    still_true = False
            if still_true:
                total_aff_corrects += 1

        print 'Number of fully corrects affordance vectors:', \
            total_aff_corrects, '\n'

        obj_tps =[]
        obj_fps = []
        obj_tns = []
        obj_fns = []
        obj_corrects = []

        obj_gt_file = base_name + 'test-' + str(i) + '-' + str(train_size) + \
            '-CA-FULL-OBJECTS.txt'
        obj_pred_file = base_name + 'test-' + str(i) +'-' + str(train_size) +\
            '-CA-FULL-OBJECTS-output.txt'
        obj_data = compare_multiclass_files(obj_gt_file, obj_pred_file)

        obj_tps.extend(obj_data[0])
        obj_fps.extend(obj_data[1])
        obj_tns.extend(obj_data[2])
        obj_fns.extend(obj_data[3])
        obj_corrects.extend(obj_data[4])

        if print_obj_info:
            print '\nObject classification:'
            print 'True Pos ', sum(obj_tps)
            print 'False Pos', sum(obj_fps)
            print 'True Neg ', sum(obj_tns)
            print 'False Neg', sum(obj_fns)
            print ''

    for a, aff in enumerate(_AFF_LABELS):
        trues = sum(total_aff_tps[a]) + sum(total_aff_tns[a])
        falses = sum(total_aff_fps[a]) + sum(total_aff_fns[a])
        correct = trues / float(trues+falses)*100.0
        print 'Overal % correct for', aff, correct

    correct_percent = total_true / float(total_true + total_false)*100.0
    print 'Overall percent correct: ', correct_percent

def check_ca_chain_results(base_name, train_size, name='CA-CHAIN'):
    print name

    num_sets = _NUM_SETS

    all_total_true = 0
    all_total_false = 0

    total_aff_tps =[]
    total_aff_fps = []
    total_aff_tns = []
    total_aff_fns = []

    for a, aff in enumerate(_AFF_LABELS):
        total_aff_tps.append([])
        total_aff_fps.append([])
        total_aff_tns.append([])
        total_aff_fns.append([])

    for i in xrange(num_sets):
        tps =[]
        fps = []
        tns = []
        fns = []
        corrects = []

        start_name = base_name+'test-'+str(i)+'-'+str(train_size)+'-'
        for a, aff in enumerate(_AFF_LABELS):
            gt_file = start_name + aff + '-CA-CHAIN.txt'
            pred_file = start_name + aff + '-CA-CHAIN-output.txt'

            tp, fp, tn, fn, res = compare_binary_files(gt_file, pred_file)
            tps.append(tp)
            fps.append(fp)
            tns.append(tn)
            fns.append(fn)
            corrects.append(res)

            total_aff_tps[a].extend(tp)
            total_aff_fps[a].extend(fp)
            total_aff_tns[a].extend(tn)
            total_aff_fns[a].extend(fn)

        (total_true, total_false) = print_tfnp_table(_AFF_LABELS, tps, fps, tns,
                                                     fns)

        all_total_true += total_true
        all_total_false += total_false


        full_labels = []
        for i in xrange(len(corrects[0])):
            img_affs = []
            for j, aff in enumerate(_AFF_LABELS):
                img_affs.append(corrects[j][i])
            full_labels.append(img_affs)

        total_corrects = 0
        for img in full_labels:
            still_true = True
            for aff in img:
                if not aff:
                    still_true = False
            if still_true:
                total_corrects += 1

        print 'Number of fully corrects affordance vectors:', total_corrects, '\n'

    for a, aff in enumerate(_AFF_LABELS):
        trues = sum(total_aff_tps[a]) + sum(total_aff_tns[a])
        falses = sum(total_aff_fps[a]) + sum(total_aff_fns[a])
        correct = trues / float(trues+falses)*100.0
        print 'Overal % correct for', aff, correct

    correct_percent = total_true / float(total_true + total_false)*100.0
    print 'Overall percent correct: ', correct_percent

def class_dist():
    for obj in ['balls', 'books', 'boxes', 'shoes', 'towels']:
        att_file = _BASE_BASE + obj + '0/attribute_labels.txt'
        aff_file = _BASE_BASE + obj + '0/affordance_labels.txt'
        att_label = obj + '-Attributes'
        build_file_hist(att_file, has_header = True, hist_label=att_label,
                        ylabel='Frequency',
                        xlabel='Size, Shape, Material, Color, Weight')
        aff_label = obj + '-Affordances'
        # TODO: can't deal with comments...
        build_file_hist(aff_file, has_header = True, hist_label=aff_label,
                        ylabel='Frequency',
                        xlabel='Pushable, Rollable, Graspable, Liftable, Dragable, Carryable')
    plotter.show()

def data_set_dists():

    att_colors = []
    att_type_count = 0
    for labels in _NEW_ATT_REAL_LABELS:
        for l in labels:
            if att_type_count == 0:
                att_colors.append('red')
            elif att_type_count == 1:
                att_colors.append('blue')
            elif att_type_count == 2:
                att_colors.append('green')
            elif att_type_count == 3:
                att_colors.append('brown')
            elif att_type_count == 4:
                att_colors.append('purple')
        att_type_count += 1

    att_file = _IMG_BASE + 'new_att_file.txt'
    # build_file_hist(att_file, has_header = True, hist_label='Attribute Distribution',
    #                 hist_color=att_colors,
    #                 ylabel='Frequency',
    #                 xlabel='Size, Shape, Material, Color, Weight')
    #                 # xlabel='Size[-] , Shape[27-30], Material[30-36], Color[37-45], Weight[-]')
    plotter.show()

    aff_file = _IMG_BASE + 'affordance_labels.txt'
    build_file_hist(aff_file, has_header = True, hist_label='Affordance Distribution',
                    ylabel='Percent of Object Instances',
                    xlabel='Pushable, Rollable, Graspable, Liftable, Dragable, Carryable, Traversable')
    plotter.show()

def attribute_prediction_vs_training_size(base_name, splits, min_train_size,
                                          max_train_size, train_inc,
                                          use_new_att=False):
    train_sizes = range(min_train_size, max_train_size+1, train_inc)
    a_prev = ''
    att_labels = _ATT_LABELS
    real_labels = _ATT_REAL_LABELS

    if use_new_att:
        att_labels = _NEW_ATT_LABELS
        real_labels = _NEW_ATT_REAL_LABELS

    plot_counter = 0
    for i, a in enumerate(att_labels):
        curves = []

        for s in xrange(splits):
            curve = []
            for t in train_sizes:
                cur_name = base_name + 'test-' + str(s) + '-' + str(t) + '-' +(a)
                att_pred_file = file(cur_name + '-output.txt', 'r')
                att_gt_file = file(cur_name + '.txt', 'r')

                if use_new_att and _NEW_ATT_TYPE[i] == 'f':
                    # Parse ground truth and predicted labels from files
                    pred_labels = [(float(d.strip().split()[0])) \
                                       for d in att_pred_file.readlines()]
                    att_pred_file.close()

                    gt_labels = [float(d.strip().split()[0]) \
                                     for d in att_gt_file.readlines()]
                    att_gt_file.close()

                    # TODO: Do regression error examination here
                    error = 0
                    for out, truth in zip(pred_labels, gt_labels):
                        error += (out-truth)**2.0
                    error /= len(pred_labels)
                    curve.append(error)
                    continue

                # Parse ground truth and predicted labels from files
                pred_labels = [sign(float(d.strip().split()[0])) \
                                   for d in att_pred_file.readlines()]
                att_pred_file.close()

                gt_labels = [sign(float(d.strip().split()[0])) \
                                 for d in att_gt_file.readlines()]
                att_gt_file.close()

                # Store values to calculate things
                tp = 0.0
                tn = 0.0
                fp = 0.0
                fn = 0.0
                for out, truth in zip(pred_labels, gt_labels):
                    if truth == out:
                        if out > 0:
                            tp += 1.0
                        else:
                            tn += 1.0
                    else:
                        if out > 0:
                            fp += 1.0
                        else:
                            fn += 1.0
                curve.append((tp,tn,fp,fn))
            curves.append(curve)

        test_errors = []
        test_error_values = []
        for j in xrange(len(train_sizes)):
            test_errors.append(0.0)
            test_error_values.append([])
        for c in curves:
            for j, x in enumerate(c):
                if use_new_att and _NEW_ATT_TYPE[i] == 'f':
                    cur_error = x
                else:
                    cur_error = ((x[2]+x[3])/sum(x))
                test_errors[j] += cur_error
                test_error_values[j].append(cur_error)

        test_error_error_bars = []
        for j in xrange(len(train_sizes)):
            test_errors[j] /= float(splits)
            x_bar = test_errors[j]
            cur_vals = test_error_values[j]
            std_dev = 0
            for x in cur_vals:
                std_dev += (x - x_bar)**2.0
            std_dev = sqrt(std_dev/float(splits))
            se = std_dev / sqrt(float(splits))
            test_error_error_bars.append(se)

        if a_prev == '' or (not a.startswith(a_prev[:-2])):
            if plot_counter > 0:
                plotter.figlegend(lines, real_labels[plot_counter-1], 'upper right')
            lines = []
            plotter.figure()
            plot_counter += 1

        if _USE_ERROR_BARS:
            lines.append(plotter.errorbar(train_sizes, test_errors,
                                                  yerr=test_error_error_bars)[0])
        else:
            lines.append(plotter.plot(train_sizes, test_errors))


        plotter.title(a[:-1])
        print 'Plotting:', a
        plotter.xlabel('Training Size')
        plotter.ylabel('Test Error')
        plotter.xlim(train_sizes[0], train_sizes[-1])
        ymin, ymax = plotter.ylim()
        ymax = 0.5
        plotter.ylim((0.0, ymax))
        plotter.draw()
        a_prev = a
    plotter.figlegend(lines, real_labels[plot_counter-1], 'upper right')
    plotter.show()
    return

def knn_attribute_prediction_vs_training_size(base_name, splits, min_train_size,
                                              max_train_size, train_inc,
                                              use_new_att=False):
    train_sizes = range(min_train_size, max_train_size+1, train_inc)
    a_prev = ''
    att_labels = _ATT_LABELS
    real_labels = _ATT_REAL_LABELS

    if use_new_att:
        att_labels = _NEW_ATT_LABELS
        real_labels = _NEW_ATT_REAL_LABELS

    plot_counter = 0
    for i, a in enumerate(att_labels):
        curves = []

        for s in xrange(splits):
            curve = []
            for t in train_sizes:
                cur_name = base_name + 'test-' + str(s) + '-' + str(t) + '-' +(a)
                att_pred_file = file(cur_name + '-output.txt', 'r')
                att_gt_file = file(cur_name + '.txt', 'r')


                if use_new_att and _NEW_ATT_TYPE[i] == 'f':
                    # Parse ground truth and predicted labels from files
                    pred_labels = [(float(d.strip().split()[0])) \
                                       for d in att_pred_file.readlines()]
                    att_pred_file.close()

                    gt_labels = [float(d.strip().split()[0]) \
                                     for d in att_gt_file.readlines()]
                    att_gt_file.close()

                    # TODO: Do regression error examination here
                    error = 0
                    for out, truth in zip(pred_labels, gt_labels):
                        error += (out-truth)**2.0
                    error /= len(pred_labels)
                    curve.append(error)
                    continue

                # Parse ground truth and predicted labels from files
                pred_labels = [(float(d.strip().split()[0])) \
                                   for d in att_pred_file.readlines()]
                att_pred_file.close()

                gt_labels = [0.5*(sign(float(d.strip().split()[0]))+1) \
                                 for d in att_gt_file.readlines()]
                att_gt_file.close()

                # Store values to calculate things
                tp = 0.0
                tn = 0.0
                fp = 0.0
                fn = 0.0
                for out, truth in zip(pred_labels, gt_labels):
                    if truth == out:
                        if out > 0:
                            tp += 1.0
                        else:
                            tn += 1.0
                    else:
                        if out > 0:
                            fp += 1.0
                        else:
                            fn += 1.0
                curve.append((tp,tn,fp,fn))
            curves.append(curve)

        test_errors = []
        test_error_values = []

        for j in xrange(len(train_sizes)):
            test_errors.append(0.0)
            test_error_values.append([])
        for c in curves:
            for j, x in enumerate(c):
                if use_new_att and _NEW_ATT_TYPE[i] == 'f':
                    cur_error = x
                else:
                    cur_error = ((x[2]+x[3])/sum(x))
                test_errors[j] += cur_error
                test_error_values[j].append(cur_error)

        test_error_error_bars = []
        for j in xrange(len(train_sizes)):
            test_errors[j] /= float(splits)
            x_bar = test_errors[j]
            cur_vals = test_error_values[j]
            std_dev = 0
            for x in cur_vals:
                std_dev += (x - x_bar)**2.0
            std_dev = sqrt(std_dev/float(splits))
            se = std_dev / sqrt(float(splits))
            test_error_error_bars.append(se)

        if a_prev == '' or (not a.startswith(a_prev[:-2])):
            if plot_counter > 0:
                plotter.figlegend(lines, real_labels[plot_counter-1],
                                  'upper right')
            lines = []
            plotter.figure()
            plot_counter += 1

        if _USE_ERROR_BARS:
            lines.append(plotter.errorbar(train_sizes, test_errors,
                                          yerr=test_error_error_bars)[0])
        else:
            lines.append(plotter.plot(train_sizes, test_errors))


        plotter.title(a[:-1])
        print 'Plotting:', a
        plotter.xlabel('Training Size')
        plotter.ylabel('Test Error')
        plotter.xlim(train_sizes[0], train_sizes[-1])
        ymin, ymax = plotter.ylim()
        ymax = 0.5
        plotter.ylim((0.0, ymax))
        plotter.draw()
        a_prev = a
    plotter.figlegend(lines, real_labels[plot_counter-1], 'upper right')
    plotter.show()
    return

def affordance_prediction_vs_training_size(base_name, splits, min_train_size,
                                           max_train_size, train_inc,
                                           name_suffix='', title_prefix='',
                                           fig_spot='lower right'):
    train_sizes = range(min_train_size, max_train_size+1, train_inc)
    plotter.figure()
    plotted_lines = []
    for i, a in enumerate(_AFF_LABELS):
        curves = []
        test_errors = []
        test_error_values = []
        for j in xrange(len(train_sizes)):
            test_errors.append(0.0)
            test_error_values.append([])
        for s in xrange(splits):
            curve = []
            for j, t in enumerate(train_sizes):
                cur_name = base_name + 'test-' + str(s) + '-' + str(t) + '-' +(a) + name_suffix
                aff_pred_file = file(cur_name + '-output.txt', 'r')
                aff_gt_file = file(cur_name + '.txt', 'r')

                # Parse ground truth and predicted labels from files
                pred_labels = [sign(float(d.strip().split()[0])) \
                                   for d in aff_pred_file.readlines()]
                aff_pred_file.close()

                gt_labels = [sign(float(d.strip().split()[0])) \
                                 for d in aff_gt_file.readlines()]
                aff_gt_file.close()

                # Store values to calculate things
                tp = 0.0
                tn = 0.0
                fp = 0.0
                fn = 0.0
                for out, truth in zip(pred_labels, gt_labels):
                    if truth == out:
                        if out > 0:
                            tp += 1.0
                        else:
                            tn += 1.0
                    else:
                        if out > 0:
                            fp += 1.0
                        else:
                            fn += 1.0
                curve.append((tp,tn,fp,fn))
                cur_error = ((fp+fn)/(tp+tn+fp+fn))
                test_errors[j] += cur_error
                test_error_values[j].append(cur_error)
            curves.append(curve)

        test_error_error_bars = []
        for j in xrange(len(train_sizes)):
            test_errors[j] /= float(splits)
            x_bar = test_errors[j]
            cur_vals = test_error_values[j]
            std_dev = 0
            for x in cur_vals:
                std_dev += (x - x_bar)**2.0
            std_dev = sqrt(std_dev/float(splits))
            se = std_dev / sqrt(float(splits))
            test_error_error_bars.append(se)

        print 'Plotting:', a
        if _USE_ERROR_BARS:
            plotted_lines.append(plotter.errorbar(train_sizes, test_errors,
                                                  yerr=test_error_error_bars)[0])
        else:
            plotted_lines.append(plotter.plot(train_sizes, test_errors))

    plotter.xlabel('Training Size')
    plotter.ylabel('Test Error')
    plotter.xlim(train_sizes[0], train_sizes[-1])
    ymin, ymax = plotter.ylim()
    ymax = 0.8
    plotter.ylim((0.0, ymax))
    plotter.title(title_prefix + 'Affordance Prediction Results')
    aff_title_labels = [a.title() for a in _AFF_LABELS]
    plotter.figlegend(plotted_lines, aff_title_labels, fig_spot,
                      numpoints=3, markerscale=0.6, ncol=2)
    plotter.draw()
    plotter.show()
    return

def affordance_prediction_roc(base_name, splits, min_train_size,
                              max_train_size, train_inc,
                              name_suffix='', title_prefix='',
                              fig_spot='lower right'):
    train_sizes = range(min_train_size, max_train_size+1, train_inc)
    plotter.figure()
    plotted_lines = []
    for i, a in enumerate(_AFF_LABELS):
        curves = []

        tps = []
        fps = []
        for j in xrange(len(train_sizes)):
            tps.append(0)
            fps.append(0)

        for s in xrange(splits):
            curve = []

            for j, t in enumerate(train_sizes):
                cur_name = base_name + 'test-' + str(s) + '-' + str(t) + '-' +(a) + name_suffix
                aff_pred_file = file(cur_name + '-output.txt', 'r')
                aff_gt_file = file(cur_name + '.txt', 'r')

                # Parse ground truth and predicted labels from files
                pred_labels = [sign(float(d.strip().split()[0])) \
                                   for d in aff_pred_file.readlines()]
                aff_pred_file.close()

                gt_labels = [sign(float(d.strip().split()[0])) \
                                 for d in aff_gt_file.readlines()]
                aff_gt_file.close()

                # Store values to calculate things
                tp = 0.0
                tn = 0.0
                fp = 0.0
                fn = 0.0
                for out, truth in zip(pred_labels, gt_labels):
                    if truth == out:
                        if out > 0:
                            tp += 1.0
                        else:
                            tn += 1.0
                    else:
                        if out > 0:
                            fp += 1.0
                        else:
                            fn += 1.0
                curve.append((tp,tn,fp,fn))
                if (tp + fp) == 0.0:
                    precision = 0
                else:
                    precision = tp/(tp+fp)
                if (tp + fn) == 0.0:
                    recall = 0
                else:
                    recall = tp/(tp+fn)
                tps[j] += precision
                fps[j] += recall
            curves.append(curve)

        for j in xrange(len(train_sizes)):
            tps[j] = tps[j] / float(splits)
            fps[j] = fps[j] / float(splits)

        print 'Plotting:', a
        plotted_lines.append(plotter.plot(fps, tps))

    plotter.xlabel('False Positives')
    plotter.ylabel('True Positives')
    plotter.xlim((0.0, 1.0))
    plotter.ylim((0.0, 1.0))
    plotter.title(title_prefix + 'Affordance Prediction Results')
    aff_title_labels = [a.title() for a in _AFF_LABELS]
    plotter.figlegend(plotted_lines, aff_title_labels, fig_spot,
                      numpoints=3, markerscale=0.6, ncol=2)
    plotter.draw()
    plotter.show()
    return

def object_classification_vs_training_size(base_name, splits, min_train_size,
                                           max_train_size, train_inc,
                                           fig_spot='lower right',
                                           ca_type='CHAIN'):
    train_sizes = range(min_train_size, max_train_size+1, train_inc)
    plotter.figure()
    plotted_lines = []

    test_errors = []
    test_error_values = []
    for j in xrange(len(train_sizes)):
        test_errors.append(0.0)
        test_error_values.append([])
    curve = []
    for s in xrange(splits):
        for j, t in enumerate(train_sizes):
            cur_name = base_name + 'test-' + str(s) + '-' + str(t) + '-CA-' + \
                ca_type + '-OBJECTS'
            aff_pred_file = file(cur_name + '-output.txt', 'r')
            aff_gt_file = file(cur_name + '.txt', 'r')

            # Parse ground truth and predicted labels from files
            pred_labels = [int(d.strip().split()[0]) for d in aff_pred_file.readlines()]
            aff_pred_file.close()

            gt_labels = [int(d.strip().split()[0]) for d in aff_gt_file.readlines()]
            aff_gt_file.close()

            # Store values to calculate things
            correct = 0
            incorrect = 0
            for out, truth in zip(pred_labels, gt_labels):
                if truth == out:
                    correct += 1.0
                else:
                    incorrect += 1.0

            cur_error = ((incorrect)/(correct+incorrect))
            test_errors[j] += cur_error
            test_error_values[j].append(cur_error)

    test_error_error_bars = []
    for j in xrange(len(train_sizes)):
        test_errors[j] /= float(splits)
        x_bar = test_errors[j]
        cur_vals = test_error_values[j]
        std_dev = 0
        for x in cur_vals:
            std_dev += (x - x_bar)**2.0
        std_dev = sqrt(std_dev/float(splits))
        se = std_dev / sqrt(float(splits))
        test_error_error_bars.append(se)

    plotter.errorbar(train_sizes, test_errors, yerr=test_error_error_bars)[0]

    plotter.xlabel('Training Size')
    plotter.ylabel('Test Error')
    plotter.xlim(train_sizes[0], train_sizes[-1])
    ymin, ymax = plotter.ylim()
    # ymax = 0.8
    plotter.ylim((0.0, ymax))
    plotter.title('Object Prediction Results')
    # plotter.figlegend(plotted_lines, aff_title_labels, fig_spot,
    #                   numpoints=3, markerscale=0.6, ncol=2)
    plotter.draw()
    plotter.show()
    return


#
# Helper functions
#
def condense_ca_full_output(base_name, splits, min_train_size, max_train_size,
                            train_step):
    train_sizes = range(min_train_size, max_train_size+1, train_step)

    for s in xrange(splits):
        for t in train_sizes:
            for aff in _AFF_LABELS:
                cur_base = base_name + 'test-' + str(s) + '-' + str(t) + '-'
                gt_out_path = cur_base + aff + '-ca-full.txt'
                print 'Opening file:', gt_out_path
                pred_out_path = cur_base + aff + '-ca-full-output.txt'
                print 'Opening file:', pred_out_path
                gt_out = file(gt_out_path, 'w')
                pred_out = file(pred_out_path, 'w')

                for obj in _OBJECT_LABELS:
                    gt_in_path = cur_base + obj + '-' + aff + '.txt'
                    pred_in_path = cur_base + obj + '-' +  aff + '-ca-full-output.txt'

                    print 'Opening file:', gt_in_path
                    gt_in = file(gt_in_path, 'r')
                    print 'Opening file:', pred_in_path
                    pred_in = file(pred_in_path, 'r')

                    gt_out.writelines(gt_in.readlines())
                    pred_out.writelines(pred_in.readlines())

                    gt_in.close()
                    pred_in.close()

                gt_out.close()
                pred_out.close()

def condense_ca_chain_output(base_name, splits, min_train_size, max_train_size,
                            train_step):
    train_sizes = range(min_train_size, max_train_size+1, train_step)

    for s in xrange(splits):
        for t in train_sizes:
            for aff in _AFF_LABELS:
                cur_base = base_name + 'test-' + str(s) + '-' + str(t) + '-'
                gt_out_path = cur_base + aff + '-CA-CHAIN.txt'
                print 'Opening file:', gt_out_path
                gt_out = file(gt_out_path, 'w')
                for obj in _OBJECT_LABELS:
                    gt_in_path = cur_base + obj + '-' + aff + '.txt'
                    print 'Opening file:', gt_in_path
                    gt_in = file(gt_in_path, 'r')
                    gt_out.writelines(gt_in.readlines())
                    gt_in.close()
                gt_out.close()


#
# These are datasets we have correctly generated
#
_BASE_0 = '/home/thermans/data/mars_lab_aff_pics/svm_lists/'
_BASE_C = '/home/thermans/data/cropped_mars/svm_lists/'

_BASE_ATT_1 = '/home/thermans/data/mars_lab_affordance_output/att-base-3-9/'
_BASE_DP_1 = '/home/thermans/data/mars_lab_affordance_output/dp-3-9/'
_BASE_FULL_1 = '/home/thermans/data/mars_lab_affordance_output/full-base-3-9/'
_BASE_CHAIN_1 = '/home/thermans/data/mars_lab_affordance_output/chain-base-3-9/'

_BASE_ATT_C = '/home/thermans/data/mars_lab_affordance_output/cropped-att-base-3-11/'
_BASE_DP_C = '/home/thermans/data/mars_lab_affordance_output/cropped-dp-3-11/'
_BASE_FULL_C = '/home/thermans/data/mars_lab_affordance_output/cropped-ca-full-3-11/'
_BASE_CHAIN_C = '/home/thermans/data/mars_lab_affordance_output/cropped-ca-chain-3-11/'

_BASE_FULL_S = '/home/thermans/data/mars_lab_affordance_output/sift-full-3-11/'
_BASE_DP_S = '/home/thermans/data/mars_lab_affordance_output/sift-dp-3-11/'
_BASE_ATT_S = '/home/thermans/data/mars_lab_affordance_output/sift-att-3-11/'
_BASE_ATT_N = '/home/thermans/data/mars_lab_affordance_output/reg_new_att-3-11/'
_BASE_ATT_W = '/home/thermans/data/mars_lab_affordance_output/att-base-weighted/'

_DP_KNN_5 = '/home/thermans/data/mars_lab_affordance_output/dp-knn-chi-5/'
_CHAIN_KNN_5 = '/home/thermans/data/mars_lab_affordance_output/chain-nn-chi-5/'
_FULL_KNN_5 = '/home/thermans/data/mars_lab_affordance_output/full-nn-chi-5/'
_ATT_KNN_5 = '/home/thermans/data/mars_lab_affordance_output/att-knn-chi-5-long/'
_ATT_HIK = '/home/thermans/data/mars_lab_affordance_output/hik-att/'
_BALLS_KNN_DP = '/home/thermans/data/mars_lab_affordance_output/no-balls-dp-knn/'


_ATT_SVM_MC = '/home/thermans/data/mars_lab_affordance_output/att-svm-multichannel/'
_ATT_KNN_MC = '/home/thermans/data/mars_lab_affordance_output/att-multichannel-3-20/'
_DP_SVM_MC = '/home/thermans/data/dp-svm-multichannel/'

_ATT_MC = '/home/thermans/data/mars_lab_affordance_output/att-multichannel-3-20/'
_FULL_MC = '/home/thermans/data/mars_lab_affordance_output/full-knn-multichannel-chi-5/'

_ATT_HYBRID = '/home/thermans/data/mars_lab_affordance_output/att-hybrid-aff-knn/'

_NOVEL_ATT = '/home/thermans/data/mars_lab_affordance_output/novel_objects/no-boxes-att-svm/'

_FULL_SVM_MC = '/home/thermans/data/mars_lab_affordance_output/full-svm-multichannel-chi/'
_FULL_KNN_MC = '/home/thermans/data/mars_lab_affordance_output/full-knn-multichannel-chi-5/'
_CHAIN_KNN_MC = '/home/thermans/data/mars_lab_affordance_output/chain-knn-multichannel-chi/'
_BASE_DP = _BASE_0
_BASE_FULL = _FULL_KNN_MC
_BASE_CHAIN = _CHAIN_KNN_5
_BASE_ATT = _ATT_KNN_MC

def main():
    splits = 5
    min_train_size = 10
    max_train_size = 260
    train_step = 10

    use_dp = False
    use_att = True
    plot_attribute_values = True
    use_knn = True

    plot_roc = False

    use_ca_full = False
    use_ca_chain = False
    condense_ca_files = False
    use_objects = False
    use_object_chain = False
    use_object_full = True

    if use_dp:
        print 'Creating dp affordance graphs'
        if plot_roc:
            affordance_prediction_roc(_BASE_DP, splits, min_train_size,
                                      max_train_size, train_step,
                                      title_prefix='Direct Perception ',
                                      fig_spot='lower right')

            return
        affordance_prediction_vs_training_size(_BASE_DP,
                                               splits, min_train_size,
                                               max_train_size, train_step,
                                               title_prefix='Direct Perception ',
                                               fig_spot='upper right')

    if use_att:
        if not plot_attribute_values:
            print 'Creating att affordance graphs'
            affordance_prediction_vs_training_size(_BASE_ATT,
                                                   splits, min_train_size,
                                                   max_train_size, train_step,
                                                   name_suffix='-att-feat',
                                                   title_prefix='Attrbute Based ',
                                                   fig_spot='upper right')
        else:
            print 'Creating attribute graphs'
            if use_knn:
                knn_attribute_prediction_vs_training_size(_BASE_ATT,
                                                          splits,
                                                          min_train_size,
                                                          max_train_size,
                                                          train_step, True)
            else:
                attribute_prediction_vs_training_size(_BASE_ATT,
                                                      splits,
                                                      min_train_size,
                                                      max_train_size,
                                                      train_step, True)

    if use_ca_full:
        if condense_ca_files:
            print 'Condensing ca_full output files'
            condense_ca_full_output(_FULL_KNN_MC, splits, min_train_size,
                                    max_train_size, train_step)
        print 'Creating ca_full graphs'
        affordance_prediction_vs_training_size(_BASE_FULL,
                                               splits, min_train_size,
                                               max_train_size, train_step,
                                               name_suffix='-ca-full',
                                               title_prefix='CA-FULL ',
                                               fig_spot='upper right')

    if use_ca_chain:
        if condense_ca_files:
            print 'Condensing ca_chain output files'
            condense_ca_chain_output(_BASE_CHAIN, splits, min_train_size,
                                     max_train_size, train_step)
        print 'Creating ca_chain graphs'
        affordance_prediction_vs_training_size(_BASE_CHAIN,
                                               splits, min_train_size,
                                               max_train_size, train_step,
                                               name_suffix='-CA-CHAIN',
                                               title_prefix='CA-CHAIN ')

    if use_objects:
        if use_object_chain:
            object_classification_vs_training_size(_BASE_CHAIN, splits, \
                                                       min_train_size, \
                                                       max_train_size, \
                                                       train_step, ca_type='CHAIN')
        if use_object_full:
            object_classification_vs_training_size(_BASE_FULL, splits, \
                                                       min_train_size, \
                                                       max_train_size, \
                                                       train_step, ca_type='FULL')

    #
    # OLD STUFF
    #

    # default
    # check_att_results(_ATT_SVM_MC, 260)
    # check_att_results(_ATT_KNN_MC, 260)
    # check_att_results(_NOVEL_ATT, 300)
    # check_dp_results(_DP_SVM_MC, 260)

    # NOTE: tukey
    # check_ca_full_knn_results(_FULL_KNN_MC, 260,'CA-Full')
    # check_ca_chain_results(_CHAIN_KNN_MC, 260, 'CA-Chain')

    # NOTE: tukey
    # check_att_results(_BASE_RBF1, 'Att-RBF1')

    # TODO: make multiple histograms, label stuff correctly
    # class_dist()

    # NOTE: these are for looking at statistics of the data
    # data_set_dists()

    return

if __name__ == '__main__':
    main()
