#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import roslib; roslib.load_manifest('attribute_aff_learning')
import rospy

_DEBUG_CLASSIFIER = False

def sign(x):
    if x < 0:
        return -1
    if x > 0:
        return 1
    return 0

class MajorityVoteClassifier:
    '''
    Learner for a majority vote classifer among n labels
    '''
    def __init__(self):
        self.labels = []
        self.label_dist = {}

    def learn(self, labels, ys):
        '''
        Method reads the given values and gets the majority option
        saving it to memory, so that test examples can be classified later
        '''
        self.label_dist = {}

        for l in labels:
            self.label_dist[l] = 0
        for y in ys:
            self.label_dist[y] += 1

    def classify(self):
        '''
        Method uses the learned labels to return the majority vote
        '''
        max_val = -1
        max_idx = -1
        for l in self.label_dist:
            cur_val = self.label_dist[l]
            if max_val < cur_val:
                max_val = cur_val
                max_idx = l
        return max_idx

class ConditionalMajorityVoteClassifier:
    '''
    Return the majority vote classification, conditioned on another variable
    '''
    def __init__(self):
        self.labels = [] # These are the labels of the initial variable
        self.response_labels = [] # These are the labels which can be returned
        self.response_label_dist = {}

    def learn(self, labels, response_labels, xs, ys):
        self.labels = labels
        self.response_labels = response_labels
        self.response_label_dist = {}

        for l in self.labels:
            cond_dist = {}
            for r in self.response_labels:
                cond_dist[r] = 0
            self.response_label_dist[l] = cond_dist

        for x,y in zip(xs, ys):
            self.response_label_dist[x][y] += 1

    def classify(self, x):

        max_val = -1
        max_idx = -1

        for l in self.response_label_dist[x]:
            cur_val = self.response_label_dist[x][l]
            if max_val < cur_val:
                max_val = cur_val
                max_idx = l

        return max_idx

    def batch_classify(self, xs, output_filename=None):
        ts = []
        print 'Have', len(xs), ' examples to classify'
        for x in xs:
            t = self.classify(x)
            ts.append(t)
            if _DEBUG_CLASSIFIER:
                rospy.loginfo('Outputting label: ' + str(t) + '\tfor input: ' +
                              str(x))

        if _DEBUG_CLASSIFIER:
            rospy.loginfo('Distribution: ' + str(self.response_label_dist))

        if output_filename is not None:
            output_file = file(output_filename, 'w')
            for t in ts:
                output_file.write(str(t)+'\n')
            output_file.close()

        return ts
