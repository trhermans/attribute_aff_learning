#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import numpy as np

class KNearestNeighbors:

    def __init__(self, k):
        self.k = k
        self.exemplars = np.asarray([])
        self.labels = np.asarray([])
        self.dist_func = self.euclidean_dist_squared
        self.bin_weight = 1.0
        self.float_weight = 1.0
        self.feat_types = []
        self.channel_weights = []
        self.channel_stops = []

    #
    # Methods to perform batch k-nn classification or regression
    #
    def classify(self, X):
        labels = []
        for i, x in enumerate(X):
            labels.append(self.classify_single(x, False)[0])
        return np.asarray(labels)

    def regress(self, X):
        regression_values = []
        for x in X:
            regression_values.append(self.classify_single(x, regress=True)[0])
        return np.asarray(regression_values)

    #
    # Core methods performing the classification / regression
    #
    def classify_single(self, x, regress=False):
        """
        Give the labels and associated score for a query example x
        """
        nn_dists = []
        nn_labels = []
        nn_ids = []

        # Do insertion sort on the exemplars to find the nearest neighbors

        for i, ex in enumerate(self.exemplars):
            d_ex = self.dist_func(x, ex)
            lab = self.labels[i]
            if len(nn_dists) < 1:
                nn_dists.append(d_ex)
                nn_labels.append(lab)
                nn_ids.append(i)
            else:
                for j, d in enumerate(nn_dists):
                    if d_ex <= d:
                        nn_dists.insert(j, d_ex)
                        nn_labels.insert(j, lab)
                        nn_ids.insert(j, i)
                        break
                if d_ex > nn_dists[-1]:
                    nn_dists.append(d_ex)
                    nn_labels.append(lab)
                    nn_ids.append(i)

        nn_labels = np.asarray(nn_labels[:self.k])
        nn_dists = np.asarray(nn_dists[:self.k])
        nn_ids = np.asarray(nn_ids[:self.k])

        if regress:
            weighted_label = self.weighted_regression(nn_labels, nn_dists)
            return (weighted_label, np.zeros(1), nn_labels, nn_dists, nn_ids)

        min_label, label_scores = self.weighted_labels(nn_labels, nn_dists)
        return (min_label, label_scores, nn_labels, nn_dists, nn_ids)

    def weighted_labels(self, labels, dists):
        """
        Return the nearest label and associated scores for all labels
        """
        # First take care of the easy case where we only have one set of labels
        if len(labels.shape) == 1:
            return self.weighted_label_single_vector(labels, dists)

        # Now take care of the multiple label case
        min_labels = []
        label_scores = []
        for i in xrange(labels.shape[1]):
            min_l, l_scores = self.weighted_label_single_vector(labels[:,i], dists)
            min_labels.append(min_l)
            label_scores.append(l_scores)

        return np.asarray(min_labels), np.asarray(label_scores)

    def weighted_regression(self, values, dists):
        """
        Return the weighted average of the values of the k nearest neighbors
        """
        # First take care of the easy case where we only have one set of values
        if len(values.shape) == 1 or (len(values.shape) == 2 and
                                      values.shape[1] == 1):
            return self.weighted_regression_single_vector(values, dists)

        # Now take care of the multiple value case
        min_values = []
        for i in xrange(values.shape[1]):
            v = values[:,i]
            min_value = self.weighted_regression_single_vector(v, dists)
            l_scores = 0.0
            min_values.append(min_value)

        return np.asarray(min_values)

    def weighted_label_single_vector(self, labels, dists):
        label_scores = {}
        label_counts = {}

        # Compute the summed distance to each of the labels, keeping track of
        # the number for each label
        for l, lab in enumerate(labels):
            d_l = dists[l]
            if label_scores.has_key(lab):
                label_scores[lab] += d_l
                label_counts[lab] += 1
            else:
                label_scores[lab] = d_l
                label_counts[lab] = 1


        # Normalize by the number of examples for each label
        # And determine the minimum score
        min_score = label_scores[label_scores.keys()[0]]
        min_label = label_scores.keys()[0]
        for key in label_scores:
            label_scores[key] = label_scores[key] / label_counts[key]
            if label_scores[key] <= min_score:
                min_score = label_scores[key]
                min_label = key

        return min_label, label_scores

    def weighted_regression_single_vector(self, values, dists):
        # In the case were all are equally close, just take the mean
        if sum(dists) == 0.0:
            return values.mean()

        weighted_ave = 0.0
        dist_prime = sum(dists) - dists
        for v, d in zip(values, dist_prime):
            weighted_ave += v*d
        weighted_ave /= sum(dist_prime)
        return weighted_ave

    #
    # Distance Functions
    #

    def euclidean_dist_squared(self, a, b):
        diff = (a-b)
        dist = sum(diff**2.0)
        return dist

    def euclidean_dist(self, a, b):
        diff = (a-b)
        dist = sum(diff**2.0)
        dist = np.sqrt(dist)
        return dist

    def chi_squared_dist(self, a, b):
        val = 0
        for a_i, b_i in zip(a,b):
            top = a_i - b_i
            if top == 0:
                continue
            top = top**2.0
            val += top/(0.5*(a_i + b_i))
        return val

    def multi_channel_chi_squared(self, x, y):
        full_dist = 0.0

        for i in xrange(len(self.channel_stops)):
            if i == 0:
                start = 0
            else:
                start = self.channel_stops[i-1]
            stop = self.channel_stops[i]
            x_c = x[start:stop]
            y_c = y[start:stop]
            channel_dist = self.chi_squared_dist(x_c, y_c)
            full_dist += self.channel_weights[i]*channel_dist

        return full_dist

    def exp_multi_channel_chi_squared(self, x, y):
        return np.exp(-self.multi_channel_chi_squared(x,y))

    def hamming_dist(self, x, y):
        score = 0
        for x_i, y_i in zip(x, y):
            if x_i != y_i:
                score += 1
        return score

    def l_infinty_metric(self, x, y):
        result = 0
        for x_i, y_i in zip(x, y):
            result = max(result, max(x_i,y_i))
        return result

    def split_att_metric(self, x, y):
        x_bin = x[self.feat_types == 'b']
        y_bin = y[self.feat_types == 'b']
        bin_dist = self.hamming_dist(x_bin, y_bin)

        x_float = x[self.feat_types == 'f']
        y_float = y[self.feat_types == 'f']
        float_dist = self.euclidean_dist(x_float, y_float)

        return self.bin_weight*bin_dist + self.float_weight*float_dist

    def estimate_binary_float_channel_weights(self):
        '''
        Compare each member of X to the others
        '''
        bin_sum = 0.0
        float_sum = 0.0
        term_count = 0.0

        bin_idxes = [self.feat_types == 'b']
        float_idxes = [self.feat_types == 'f']

        X = self.exemplars

        num_vectors = len(X)
        for i in xrange(num_vectors):
            x = X[i]
            x_bin = x[bin_idxes]
            x_float = x[float_idxes]
            for j in range(i+1, num_vectors):
                y = X[j]
                y_bin = y[bin_idxes]
                y_float = y[float_idxes]
                bin_sum += self.hamming_dist(x_bin, y_bin)
                float_sum += self.euclidean_dist(x_float, y_float)
                term_count += 1.0

        self.bin_weight = 1.0 / (bin_sum/term_count)
        self.float_weight = 1.0 / (float_sum/term_count)
        # print 'bin weight is', self.bin_weight
        # print 'float weight is', self.float_weight

    def estimate_mixed_channel_weights(self):
        '''
        Compare each member of X to the others
        '''
        X = self.exemplars
        num_vectors = len(X)

        self.channel_weights = []

        for i in xrange(len(self.channel_stops)):
            if i == 0:
                start = 0
            else:
                start = self.channel_stops[i-1]
            stop = self.channel_stops[i]

            term_count = 0
            channel_sum = 0.0

            for j in xrange(num_vectors):
                x = X[j]
                x_c = x[start:stop]

                for k in range(j+1, num_vectors):
                    y = X[k]
                    y_c = y[start:stop]
                    channel_sum += self.chi_squared_dist(x_c, y_c)
                    term_count += 1.0

            self.channel_weights.append(1.0/(channel_sum/term_count))
            #print 'Weight for channel', i, 'is:', self.channel_weights[i]
