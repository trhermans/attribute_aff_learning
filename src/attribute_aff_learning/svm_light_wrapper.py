#!/usr/bin/env python
# Software License Agreement (BSD License)
#
#  Copyright (c) 2010, Georgia Institute of Technology
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#  * Neither the name of the Georgia Institute of Technology nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

import roslib; roslib.load_manifest('attribute_aff_learning')
import rospy
import subprocess

LEARN_EXEC = '/extern/svm_multiclass/svm_light/svm_learn'
CLASSIFY_EXEC = '/extern/svm_multiclass/svm_light/svm_classify'
MULTICLASS_LEARN_EXEC = '/extern/svm_multiclass/svm_multiclass_learn'
MULTICLASS_CLASSIFY_EXEC = '/extern/svm_multiclass/svm_multiclass_classify'

def sign(x):
    if x < 0:
        return -1
    if x > 0:
        return 1
    return 0

class SVMLight:
    '''
    Wrapper class for calling SVM_light executables from within ROS
    '''
    def __init__(self):
        self.example_file = ''
        self.training_file = ''
        self.testing_file = ''
        self.model_file = ''
        self.output_file = ''

        # Learning options
        self.use_regression = False
        self.use_preference = False
        # Use this to change training weights of pos / neg examples
        self.j_value = 1.0
        # TODO: Use this to weight more heavily the margin over training error
        self.c_value = 1.0
        # Paramters for user defined kernel
        self.u_value = ''

        # Kernel parameters
        self.kernel_type = 0
        self.d_value = 3
        self.gamma_value = 0.3
        self.s_value = 0.3
        self.r_value = 0.3
        self.training_v_value = 0
        self.testing_v_value = 1
        self.multiclass_c_value = 0.02

        # Find the executables using ROS
        self.base_path = roslib.packages.get_pkg_dir('attribute_aff_learning')
        self.learn_exec = self.base_path + LEARN_EXEC
        self.classify_exec = self.base_path + CLASSIFY_EXEC
        self.multiclass_learn_exec = self.base_path + MULTICLASS_LEARN_EXEC
        self.multiclass_classify_exec = self.base_path + MULTICLASS_CLASSIFY_EXEC


    def learn(self):
        '''
        Method calls the SVM_light training executable
        '''
        command = [self.learn_exec]
        kernel = ['-t', str(self.kernel_type)]
        command.extend(kernel)
        verbosity = ['-v', str(self.training_v_value)]
        command.extend(verbosity)
        if self.kernel_type == 1: # polynomial
            arg_d = ['-d', str(self.d_value)]
            command.extend(arg_d)
        if self.kernel_type == 2:
            arg_g = ['-g', str(self.gamma_value)]
            command.extend(arg_g)
        if self.kernel_type == 3 or self.kernel_type == 1:
            arg_s = ['-s', str(self.s_value)]
            arg_r = ['-r', str(self.r_value)]
            command.extend(arg_s)
            command.extend(arg_r)
        # Manually defined chi-square kernel here
        # if self.kernel_type == 4:
        #     pass

        if self.use_regression:
            command.append('-z')
            command.append('r')
        elif self.use_preference:
            command.append('-z')
            command.append('p')

        # Reweight positive / negative training examples
        command.extend(['-j', str(self.j_value)])

        # Tradeoff between margin and training error:
        # c_value = ['-c', str(self.c_value)]
        # command.extend(c_value)

        # Add user specificed option
        if self.u_value != '':
            command.extend(['-u', str(self.u_value)])

        command.append(self.train_file)
        command.append(self.model_file)

        # Run the learning exec
        # rospy.loginfo('running command: ' + str(command))
        sb = subprocess.Popen(command, shell=False)
        # Comment this out to make it multithreaded
        sb.wait()

    def classify(self):
        '''
        Method calls the SVM_light classification executable
        '''
        command = [self.classify_exec]
        verbosity = ['-v', str(self.testing_v_value)]
        command.extend(verbosity)
        command.append(self.testing_file)
        command.append(self.model_file)
        command.append(self.output_file)

        # Run the learning exec
        # rospy.loginfo('running command: ' + str(command))
        sb = subprocess.Popen(command, shell=False)
        # Comment this out to make it multithreaded
        sb.wait()

    def multiclass_learn(self):
        '''
        Method calls the SVM_light training executable
        '''
        command = [self.multiclass_learn_exec]
        kernel = ['-t', str(self.kernel_type)]
        command.extend(kernel)
        verbosity = ['-v', str(self.training_v_value)]
        command.extend(verbosity)
        c_value = ['-c', str(self.multiclass_c_value)]
        command.extend(c_value)

        if self.kernel_type == 1:
            arg_d = ['-d', str(self.d_value)]
            command.extend(arg_d)
        if self.kernel_type == 2:
            arg_g = ['-g', str(self.gamma_value)]
            command.extend(arg_g)
        if self.kernel_type == 3 or self.kernel_type == 1:
            arg_s = ['-s', str(self.s_value)]
            arg_r = ['-r', str(self.r_value)]
            command.extend(arg_s)
            command.extend(arg_r)
        # Manually defined chi-square kernel here
        # if self.kernel_type == 4:
        #     pass
        command.append(self.train_file)
        command.append(self.model_file)
        # rospy.loginfo('running command: ' + str(command))
        # Run the learning exec
        sb = subprocess.Popen(command, shell=False)
        # Comment this out to make it multithreaded
        sb.wait()

    def multiclass_classify(self):
        '''
        Method calls the SVM_light classification executable
        '''
        command = [self.multiclass_classify_exec]
        verbosity = ['-v', str(self.testing_v_value)]
        command.extend(verbosity)
        command.append(self.testing_file)
        command.append(self.model_file)
        command.append(self.output_file)

        # Run the learning exec
        # rospy.loginfo('running command: ' + str(command))
        sb = subprocess.Popen(command, shell=False)
        # Comment this out to make it multithreaded
        sb.wait()

    #
    # I/O methods
    #
    def read_predictions(self):
        data_file = file(self.output_file, 'r')
        predictions = [float(d.strip()) > 0 for d in data_file.readlines()]
        data_file.close()
        return predictions

    def read_att_predictions(self, att_type='b'):
        data_file = file(self.output_file, 'r')
        if att_type == 'b':
            predictions = [sign(float(d.strip().split()[0])) for d in
                           data_file.readlines()]
        else:
            predictions = [float(d.strip().split()[0]) for d in data_file.readlines()]
        data_file.close()
        return predictions

    def read_gt(self):
        data_file = file(self.testing_file, 'r')
        gt = [float(x[0]) > 0 for x in \
                  [r.split() for r in [l.strip() for l in data_file.readlines()]]]
        data_file.close()
        return gt

    def write_wrongs(self):
        gt = self.read_gt()
        predictions = self.read_predictions()
        correct = []

        for i in xrange(len(gt)):
            correct.append(gt[i] == predictions[i])
        return correct

    def read_multiclass_predictions(self):
        data_file = file(self.output_file, 'r')
        predictions = [int(d.strip().split()[0]) for d in data_file.readlines()]
        data_file.close()
        return predictions

#
# Example file format
#
# <line> .=. <target> <feature>:<value> ... <feature>:<value> # <info>
# <target> .=. +1 | -1 | 0 | <float>
# <feature> .=. <integer> | "qid"
# <value> .=. <float>
# <info> .=. <string>
#
# Learning options:
#          -z {c,r,p}  - select between classification (c), regression (r), and
#                        preference ranking (p) (see [Joachims, 2002c])
#                        (default classification)
#          -c float    - C: trade-off between training error
#                        and margin (default [avg. x*x]^-1)
#          -w [0..]    - epsilon width of tube for regression
#                        (default 0.1)
#          -j float    - Cost: cost-factor, by which training errors on
#                        positive examples outweight errors on negative
#                        examples (default 1) (see [Morik et al., 1999])
#          -b [0,1]    - use biased hyperplane (i.e. x*w+b0) instead
#                        of unbiased hyperplane (i.e. x*w0) (default 1)
#          -i [0,1]    - remove inconsistent training examples
#                        and retrain (default 0)
# Performance estimation options:
#          -x [0,1]    - compute leave-one-out estimates (default 0)
#                        (see [5])
#          -o [0..2]   - value of rho for XiAlpha-estimator and for pruning
#                        leave-one-out computation (default 1.0)
#                        (see [Joachims, 2002a])
#          -k [0..100] - search depth for extended XiAlpha-estimator
#                        (default 0)
# Transduction options (see [Joachims, 1999c], [Joachims, 2002a]):
#          -p [0..1]   - fraction of unlabeled examples to be classified
#                        into the positive class (default is the ratio of
#                        positive and negative examples in the training data)
#
# Kernel info
#        -t int      - type of kernel function
#                       0: linear (default)
#                       1: polynomial (s a*b+c)^d
#                       2: radial basis function exp(-gamma ||a-b||^2)
#                       3: sigmoid tanh(s a*b + c)
#                       4: user defined kernel from kernel.h
#        -d int      - parameter d in polynomial kernel
#        -g float    - parameter gamma in rbf kernel
#        -s float    - parameter s in sigmoid/poly kernel
#        -r float    - parameter c in sigmoid/poly kernel
#        -u string   - parameter of user defined kernel
