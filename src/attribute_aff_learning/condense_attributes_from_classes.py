#!/usr/bin/env python
import sys
import subprocess

BASE_FOLDERS = ['/home/thermans/data/cropped_mars/balls0/',
                '/home/thermans/data/cropped_mars/books0/',
                '/home/thermans/data/cropped_mars/boxes0/',
                '/home/thermans/data/cropped_mars/mugs0/',
                '/home/thermans/data/cropped_mars/shoes0/',
                '/home/thermans/data/cropped_mars/towels0/']
BASE_COUNTS = [67, 66, 65, 56, 56, 65]
OUTPUT_BASE = '/home/thermans/data/cropped_mars/imgs/'
LOC_OUTPUT_FILE = OUTPUT_BASE + 'img_locs.txt'

def linkImages():
    output_count = 0

    for i, folder in enumerate(BASE_FOLDERS):
        # Get input file name

        for j in xrange(BASE_COUNTS[i]):
            input_link = folder
            input_link += str(j) + '.png'

            # Get output filename
            output_link = OUTPUT_BASE + str(output_count) + ".png"
            output_count += 1

            print "ln -s", input_link, output_link
            subprocess.Popen(["ln", '-s', input_link, output_link], shell=False)

def copyImages():
    output_count = 0

    for i, folder in enumerate(BASE_FOLDERS):
        # Get input file name

        for j in xrange(BASE_COUNTS[i]):
            input_link = folder
            input_link += str(j) + '.png'

            # Get output filename
            output_link = OUTPUT_BASE + str(output_count) + ".png"
            output_count += 1

            print "cp", input_link, output_link
            subprocess.Popen(["cp", input_link, output_link], shell=False)


def makeImageLocFile():
    data_out = file(LOC_OUTPUT_FILE, 'w')
    for i, folder in enumerate(BASE_FOLDERS):
        # Get input file name

        for j in xrange(BASE_COUNTS[i]):
            image_loc = folder
            image_loc += str(j) + '.png'
            data_out.write(image_loc)
            data_out.write('\n')
    data_out.close()

if __name__ == "__main__":
    pass
    # linkImages()
    # copyImages()
    # makeImageLocFile()
