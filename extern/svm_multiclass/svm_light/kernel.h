/************************************************************************/
/*                                                                      */
/*   kernel.h                                                           */
/*                                                                      */
/*   User defined kernel function. Feel free to plug in your own.       */
/*                                                                      */
/*   Copyright: Thorsten Joachims                                       */
/*   Date: 16.12.97                                                     */
/*                                                                      */
/************************************************************************/

/* KERNEL_PARM is defined in svm_common.h The field 'custom' is reserved for */
/* parameters of the user defined kernel. You can also access and use */
/* the parameters of the other kernels. Just replace the line
             return((double)(1.0));
   with your own kernel. */

  /* Example: The following computes the polynomial kernel. sprod_ss
              computes the inner product between two sparse vectors.

      return((CFLOAT)pow(kernel_parm->coef_lin*sprod_ss(a,b)
             +kernel_parm->coef_const,(double)kernel_parm->poly_degree));
  */

/* If you are implementing a kernel that is not based on a
   feature/value representation, you might want to make use of the
   field "userdefined" in SVECTOR. By default, this field will contain
   whatever string you put behind a # sign in the example file. So, if
   a line in your training file looks like

   -1 1:3 5:6 #abcdefg

   then the SVECTOR field "words" will contain the vector 1:3 5:6, and
   "userdefined" will contain the string "abcdefg". */

#include <math.h>
#include <string.h>

/* plug in you favorite kernel */

double multichannel_chi_squared_kernel(KERNEL_PARM *kernel_parm, SVECTOR *a,
                                       SVECTOR *b)
{
  // TODO: This shit is super hard coded, fix it later
  // Chi-Square
  register double channel1_sum = 0.0;
  register double channel2_sum = 0.0;
  register double channel3_sum = 0.0;
  register WORD *ai, *bj;
  double sum = 0.0;
  double numerator;

  // NOTE: Hardcoded these values from the k-nn implementation
  const double channel1_weight = 0.00000332607870544; // 3.32607870544e-06
  const double channel2_weight = 0.000152050056792;
  const double channel3_weight = 0.000645552676254;
  ai = a->words;
  bj = b->words;
  const int break_point1 = 432; // LAB Histogram
  const int break_point2 = break_point1 + 256; // Texton filter bank
  const int break_point3 = break_point2 + 512; // SIFT Codewords

  while (ai->wnum && bj->wnum)
  {
    if(ai->wnum > bj->wnum)
    {
      bj++;
    }
    else if(ai->wnum < bj->wnum)
    {
      ai++;
    }
    else
    {
      if (ai->wnum < break_point1)
      {
        numerator = ai->weight - bj->weight;
        channel1_sum += (numerator*numerator) / (ai->weight + bj->weight);
      }
      else if (ai->wnum < break_point2)
      {
        numerator = ai->weight - bj->weight;
        channel2_sum += (numerator*numerator) / (ai->weight + bj->weight);
      }
      else if (ai->wnum < break_point3)
      {
        numerator = ai->weight - bj->weight;
        channel3_sum += (numerator*numerator) / (ai->weight + bj->weight);
      }
      ai++;
      bj++;
    }
  }

  sum = (channel1_sum*channel1_weight + channel2_sum*channel2_weight +
         channel3_sum*channel3_weight);

  return exp(-0.5*sum);
}

double hik_kernel(KERNEL_PARM *kernel_parm, SVECTOR *a, SVECTOR *b)
{

  // HIK
  register double sum = 0.0;
  register WORD *ai, *bj;
  ai = a->words;
  bj = b->words;

  while (ai->wnum && bj->wnum)
  {
    if(ai->wnum > bj->wnum)
    {
      bj++;
    }
    else if(ai->wnum < bj->wnum)
    {
      ai++;
    }
    else
    {
      if (ai->weight <= bj->weight)
      {
        sum += ai->weight;
      }
      else
      {
        sum += bj->weight;
      }
      ai++;
      bj++;
    }
  }

  return ((double)(sum));
}

double multichannel_bin_float_kernel(KERNEL_PARM *kernel_parm, SVECTOR *a,
                                     SVECTOR *b)
{
  // TODO: This shit is super hard coded, fix it later
  register double binary_sum = 0.0;
  register double float_sum = 0.0;
  register WORD *ai, *bj;
  double sum = 0.0;

  char feat_types[23] = {'f','f','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','f'};

  const double binary_weight = 0.162456504515;
  const double float_weight = 0.0393804164216;

  double a_val = 0;
  double b_val = 0;
  int idx = 0;
  ai = a->words;
  bj = b->words;

  while (ai->wnum && bj->wnum)
  {
    if(ai->wnum > bj->wnum)
    {
      idx = bj->wnum;
      a_val = ai->weight;
      bj++;
    }
    else if(ai->wnum < bj->wnum)
    {
      idx = ai->wnum;
      b_val = 0;
      ai++;
    }
    else
    {
      idx = ai->wnum;
      a_val = ai->weight;
      b_val = bj->weight;
      ai++;
      bj++;
    }
    if (feat_types[idx] == 'b') // Hamming distance on binary
    {
      if (a_val != b_val) binary_sum += 1;
    }
    else // Floating point use euclidean distance
    {
      double d = (a_val - b_val);
      float_sum += sqrt(d*d);
    }
  }

  sum = (binary_sum*binary_weight + float_sum*float_weight);

  return exp(-0.5*sum);
}

double custom_kernel(KERNEL_PARM *kernel_parm, SVECTOR *a, SVECTOR *b)
{
  // Switch based on the kernel_parm parameter to choose from these
  // kernels at run time
  char* to_use = kernel_parm->custom;
  if (strcmp(to_use, "mc-chi") == 0)
  {
    return multichannel_chi_squared_kernel(kernel_parm, a, b);
  }
  if (strcmp(to_use, "hik") == 0)
  {
    return hik_kernel(kernel_parm, a, b);
  }
  if (strcmp(to_use, "mc-bin-float") == 0)
  {
    return multichannel_bin_float_kernel(kernel_parm, a, b);
  }
}
